/* eslint-disable @typescript-eslint/no-var-requires */
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const Run = require('run-sdk');

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

const runInstances = {};

admin.initializeApp();

const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

// build multiple CRUD interfaces:
app.get('/listing-manager/:listingManagerId/:network?', async  (req, res) => {
	const {
		listingManagerId,
		network = 'main',
	} = req.params;

	try {
		const run = await loadRun(listingManagerId, network);

		const response = run && {
			ownerPubKey: run.owner.pubkey,
			ownerAddress: run.owner.address,
			purseAddress: run.purse.address,
		};

		console.log(response);

		res.send(response);
	} catch(e) {
		console.log(e);
		res.send(null);
	}
});

// build multiple CRUD interfaces:
// app.get('/listings/:listingManagerId/:network?', (req, res) => {
// 	const {
// 		listingManagerId,
// 		network = 'main',
// 	} = req.params;


// });

// Expose Express API as a single Cloud Function:
exports.api = functions.https.onRequest(app);

async function loadRun(runId, network) {
	if(runInstances[runId]) {
		return runInstances[runId];
	}

	try {
		const keys = (await admin.firestore().doc(`/listing-managers/${runId}`).get()).data();

		if(!keys) {
			return null;
		}

		console.log(keys);

		const run = new Run({
			network,
			trust: '*', // TODO
			owner: keys.ownerPrivKey,
			purse: keys.pursePrivKey,
		});

		runInstances[runId] = run;

		return run;
	} catch(e) {
		console.log(e);
		return null;
	}
}
