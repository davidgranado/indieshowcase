/* eslint-disable */
const bsv = require('bsv');
const mnemonic = require('bsv/mnemonic');

const m = mnemonic.fromRandom();
const phrase = m.phrase;
const privateKey = bsv.PrivateKey.fromString(m.toHDPrivateKey().privateKey.toString())
const publicKey = bsv.PublicKey.fromPrivateKey(privateKey);
const address = bsv.Address.fromPublicKey(publicKey, 'testnet');
const address2 = bsv.Address.fromPrivateKey(privateKey, 'testnet');

console.log(`
	phrase: ${phrase}
	privateKey: ${privateKey}
	publicKey: ${publicKey}
	address: ${address}
	address2: ${address2}
	run purse address: '17XS4idapedJHduJHUKu6RXsKLfpcvAwor'
	run purse key: 'KyyhYRJs1rHs55VLT77KUZd9NQDk1qbYe3QbhfHUr6kitwMEj3DD'
	run owner address: '1JCFPC46Z4tvUoB9pn4Mp9a7y2V9XFjAvr'
	run owner key: 'L4QCb6eRKDsTBHyfZLQsAJwhGyC2b6oPYJbiM3QvkQe2aMBkkCnH'
`);

class CardA extends Jig {
	static weight = 1;

	init() {
		this.name = 'CardA';
	}
}

class CardB extends Jig {
	static weight = 1;

	init() {
		this.name = 'CardB';
	}
}

class CardC extends Jig {
	static weight = 3;
	init() {
		this.name = 'CardC';
	}
}

class CardGenerator extends Jig {
	static generate() {
		const num = this.random();
		let card;

		const total = CardA.weight + CardB.weight + CardC.weight;
		const CardAMax = CardA.weight / total;
		const CardBMax = CardB.weight / total + CardAMax;

		if(num < CardAMax) {
			card = new CardA();
		} else if(num < CardBMax) {
			card = new CardB();
		} else {
			card = new CardC();
		}

		this.lastCard = card;

		return card;
	}

	static locationSum() {
		return this.lastCard?.location
			.split('')
			.map(c => c.charCodeAt(0))
			.reduce((c, sum) => sum + c, 0) || 0;
	}

	static random() {
		// Adding sum from last card address to
		// introduce additional variability
		this.seed = this.seed + this.locationSum();

		// Some deterministic, psuedo-random number
		// generation I found on the interwebz
		this.seed = this.seed & 0xffffffff;
		this.seed = (this.seed + 0x7ed55d16 + (this.seed << 12)) & 0xffffffff;
		this.seed = (this.seed ^ 0xc761c23c ^ (this.seed >>> 19)) & 0xffffffff;
		this.seed = (this.seed + 0x165667b1 + (this.seed << 5)) & 0xffffffff;
		this.seed = ((this.seed + 0xd3a2646c) ^ (this.seed << 9)) & 0xffffffff;
		this.seed = (this.seed + 0xfd7046c5 + (this.seed << 3)) & 0xffffffff;
		this.seed = (this.seed ^ 0xb55a4f09 ^ (this.seed >>> 16)) & 0xffffffff;
		return (this.seed & 0xfffffff) / 0x10000000;
	}
}

CardGenerator.seed = 49734321;
CardGenerator.lastCard = null;

CardGenerator.deps = {
	CardA,
	CardB,
	CardC,
};


// class Dragon extends Jig {
// 	setName(name) {
// 	this.name = name
// 	}
// }

// Dragon.metadata = { emoji: '🐉',
// base64Img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAAB+FBMVEUAAAA/mUPidDHiLi5Cn0XkNTPmeUrkdUg/m0Q0pEfcpSbwaVdKskg+lUP4zA/iLi3msSHkOjVAmETdJSjtYFE/lkPnRj3sWUs8kkLeqCVIq0fxvhXqUkbVmSjwa1n1yBLepyX1xxP0xRXqUkboST9KukpHpUbuvRrzrhF/ljbwaljuZFM4jELaoSdLtElJrUj1xxP6zwzfqSU4i0HYnydMtUlIqUfywxb60AxZqEXaoifgMCXptR9MtklHpEY2iUHWnSjvvRr70QujkC+pUC/90glMuEnlOjVMt0j70QriLS1LtEnnRj3qUUXfIidOjsxAhcZFo0bjNDH0xxNLr0dIrUdmntVTkMoyfL8jcLBRuErhJyrgKyb4zA/5zg3tYFBBmUTmQTnhMinruBzvvhnxwxZ/st+Ktt5zp9hqota2vtK6y9FemNBblc9HiMiTtMbFtsM6gcPV2r6dwroseLrMrbQrdLGdyKoobKbo3Zh+ynrgVllZulTsXE3rV0pIqUf42UVUo0JyjEHoS0HmsiHRGR/lmRz/1hjqnxjvpRWfwtOhusaz0LRGf7FEfbDVmqHXlJeW0pbXq5bec3fX0nTnzmuJuWvhoFFhm0FtrziBsjaAaDCYWC+uSi6jQS3FsSfLJiTirCOkuCG1KiG+wSC+GBvgyhTszQ64Z77KAAAARXRSTlMAIQRDLyUgCwsE6ebm5ubg2dLR0byXl4FDQzU1NDEuLSUgC+vr6urq6ubb29vb2tra2tG8vLu7u7uXl5eXgYGBgYGBLiUALabIAAABsElEQVQoz12S9VPjQBxHt8VaOA6HE+AOzv1wd7pJk5I2adpCC7RUcHd3d3fXf5PvLkxheD++z+yb7GSRlwD/+Hj/APQCZWxM5M+goF+RMbHK594v+tPoiN1uHxkt+xzt9+R9wnRTZZQpXQ0T5uP1IQxToyOAZiQu5HEpjeA4SWIoksRxNiGC1tRZJ4LNxgHgnU5nJZBDvuDdl8lzQRBsQ+s9PZt7s7Pz8wsL39/DkIfZ4xlB2Gqsq62ta9oxVlVrNZpihFRpGO9fzQw1ms0NDWZz07iGkJmIFH8xxkc3a/WWlubmFkv9AB2SEpDvKxbjidN2faseaNV3zoHXvv7wMODJdkOHAegweAfFPx4G67KluxzottCU9n8CUqXzcIQdXOytAHqXxomvykhEKN9EFutG22p//0rbNvHVxiJywa8yS2KDfV1dfbu31H8jF1RHiTKtWYeHxUvq3bn0pyjCRaiRU6aDO+gb3aEfEeVNsDgm8zzLy9egPa7Qt8TSJdwhjplk06HH43ZNJ3s91KKCHQ5x4sw1fRGYDZ0n1L4FKb9/BP5JLYxToheoFCVxz57PPS8UhhEpLBVeAAAAAElFTkSuQmCC',
// lz: `84 09 2e 30 5c 20 60 09 20 b6 02 73 1e 98 0e 80
// b0 03 b8 41 62 04 9c 01 0d 90 16 80 69 00 0d a0
// 21 40 e4 01 50 02 c0 1d 01 06 07 a4 40 10 01 7b
// fd 05 0e e0 32 40 55 80 16 00 45 01 20 44 56 13
// 80 40 01 9a 78 85 90 05 04 10 06 40 47 80 c6 00
// 98 01 c8 d9 40 0d 46 0c aa 9a 06 d2 4b f1 40 14
// 68 05 34 c1 96 00 06 80 00 5a 65 56 2c 18 00 da
// 82 d6 2a 00 b0 4e a2 48 4e 00 c0 c1 0e 32 00 2c
// 2c 8a 92 68 66 00 68 fa b8 c2 20 4c c0 54 04 6c
// 08 c1 00 a6 ce 36 00 a4 f6 5e d0 de b0 86 c2 04
// c1 ee 00 74 54 56 b0 bc 61 92 00 c0 c2 52 60 ed
// e6 8a 28 92 c1 e5 18 4e ed 0c 04 86 ea 00 04 a2
// 1c 00 c1 c1 48 5e ca ba 00 54 ba 92 19 2c 1e 00
// 6e 00 7a e2 c1 a2 54 b8 43 b0 00 f9 18 8c 00 f7
// 34 9e 68 9b da cf 47 f7 4e 47 47 2c 0b 0c db 95
// 23 8c 01 08 d8 38 57 00 9a 60 c6 1d 12 85 21 e1
// 06 27 5a 0c 17 a3 98 13 ed 26 88 3c ae 1c 84 d4
// b5 00 6a cc 3b 52 45 52 00 e1 80 83 30 5e 9c 24
// d1 af 89 8a df da b1 5f a4 1a a8 c6 74 65 52 62
// 05 34 e4 8e 61 50 c0 9e 18 35 4e 14 da 57 32 88
// 26 cf d7 11 41 0c f0 62 04 8e 24 dd 49 9b 80 4e
// 10 64 65 6a 8d 36 00 06 83 61 c1 45 9c 72 48 32
// 01 a0 41 33 ee 44 86 05 62 08 44 76 00 c4 29 ec
// 7b 48 2c 58 33 a5 94 c3 30 28 01 4b 53 0e 43 52
// 18 24 87 72 8a 4a 63 29 5d b5 18 c9 98 b7 71 4f
// 86 a6 73 66 28 5d 4d 94 ca a0 5c ed 02 1d d7 11
// 27 8e 13 d1 2c b8 3b 5c 41 50 f9 e7 04 1d 18 5e
// 18 8b b4 0b 01 67 18 60 15 30 2c 0c 53 13 9e 71
// 2f 19 dd 0a a2 af d0 f0 21 18 7f f4 e3 59 41 10
// 9e b1 1a b8 97 bd 50 cd 18 42 96 46 23 1a 2b 8c
// 49 06 30 84 35 e2 03 34 14 03 d1 84 8e aa 06 f1
// eb 08 24 28 08 a0 53 60 0c 1b 04 06 a0 5d e2 c1
// 37 5d 01 02 39 69 d8 80 67 c4 3c 98 41 c2 41 60
// 85 06 ca c1 0c 7d f1 14 30 f7 03 8c 70 51 0c 73
// 20 00 62 d4 07 01 9c d0 02 2a 89 d1 60 88 81 09
// b8 89 0e 22 50 b3 5c 62 36 0e 38 a4 19 60 61 e3
// 6e b8 03 17 70 61 10 6e 25 c0 53 c4 e4 19 10 3c
// 1c 2a 28 a7 48 09 a3 5c 08 08 12 6d 30 44 2a 62
// 24 0d 32 94 02 52 10 d5 51 a8 b0 14 c9 9c 04 77
// e4 17 68 9a c0 1d 38 60 02 06 80 65 6a 18 07 85
// c4 b9 97 76 bd 40 13 da 00 8d 16 60 89 0c 00 83
// 71 e8 10 12 40 20 85 6a f0 82 8c d4 d3 99 32 18
// 13 5c b9 41 77 74 47 1b 90 a4 8d 05 c0 25 5f f4
// 02 17 8c 40 9b 0c 60 41 1d 30 a5 da 21 81 92 7e
// 58 10 6e ce c7 1c c4 31 05 58 43 80 68 51 80 06
// da 21 17 10 01 81 65 34 40 11 bd 58 58 48 81 46
// 76 88 26 99 0c 11 19 73 ed a3 68 fa 26 0c 62 80
// 0e 65 09 86 65 21 87 03 d8 b8 03 68 30 85 61 98
// d0 0b 68 e0 81 13 79 06 3e c4 a6 3a c8 48 59 02
// 74 d6 01 4c 10 10 41 61 d7 20 d9 85 06 92 42 13
// 9e f1 19 19 60 42 49 08 82 18 70 41 17 58 85 c4
// 04 70 d6 02 18 a4 d3 32 29 e7 73 48 46 13 aa 49
// 1e 51 ad e4 48 24 c3 4b 10 10 44 2f c3 e2 18 21
// 46 97 ae 20 02 dc 04 d3 08 4e 01 6c 40 88 4c 0b
// e0 00 13 56 c4 8b 9d 85 72 34 f3 15 24 7c 40 1b
// 14 41 af 31 1a 1b 51 7c 80 17 70 a5 8e 5d 3b 46
// 49 ca 22 19 92 a1 1a 6e b8 a7 61 2d 12 0c 9c a0
// a6 1a 39 82 30 dc 62 05 40 a0 17 3a 21 86 49 dc
// c5 14 e8 e0 de 53 61 e2 67 80 07 4d 82 e9 17 5f
// b4 60 60 40 06 0d 94 51 41 67 11 85 77 f4 04 18
// 75 31 a3 88 f0 25 14 9c 22 78 c4 39 13 24 31 45
// 6a 4c 8d 85 9d c1 18 0e 61 40 26 ca 41 13 4c 41
// 1d 77 41 a1 27 78 26 19 09 50 88 05 51 e3 35 7c
// c1 14 cc e1 01 53 83 83 36 dc c3 17 48 05 94 13
// 00 c3 6e 9c 04 1a ee 11 5d 48 39 c2 3b 82 e6 c8
// c0 a8 42 30 04 3b 19 98 20 8c 09 24 8d 64 d1 06
// 30 52 ea 92 ee 71 a8 2d 08 e3 01 7b 96 60 92 81
// 57 48 16 c6 76 d0 05 89 48 e1 01 61 d9 65 60 b8
// 80 80 6b 60 b9 01 b8 47 02 0d a5 5f bc 20 08 41
// 41 e0 04 dc 05 03 7c 11 1f 0c 24 a0 4d 68 e4 04
// 34 85 1c 60 59 a1 59 ca 85 a1 92 cb 00 53 04 83
// 02 9c 06 4c 08 74 49 67 a8 c6 23 56 c4 ab c1 30
// 13 14 09 46 0c 76 95 80 8e 20 86 72 5a 80 0b 0e
// 08 a0 30 28 20 46 13 0c 23 70 c4 81 0c a8 10 44
// 8d 04 40 03 40 0c d0 c0 03 07 60 a0 22 83 26 cd
// 70 0f 0b 9a ac 04 00 0b 39 90 40 67 82 35 45 3f
// bb 80 c3 1c 15 b8 f1 83 11 82 00 81 01 28 2c 60
// 85 72 60 75 86 39 27 08 12 0c 40 f0 21 1c 1a 24
// de 01 0e 6c b4 53 08 57 2d 20 44 06 48 20 02 04
// 2a 08 61 08 37 aa f2 01 99 8e 30 d1 07 35 34 41
// 46 3f 6d 00 c2 1d 24 f0 80 00 2f 98 c8 70 09 e6
// 1c c1 01 52 88 c1 d6 12 b9 61 f1 c3 65 80 20 0a
// 06 c1 58 01 37 d6 68 43 8f 6a b6 a0 06 b0 74 d0
// e1 19 5e 80 e1 0e 51 1c e0 c6 04 b0 3c e3 85 60
// d8 20 83 20 92 04 c2 1c 44 42 68 61 1a 2d 40 01
// 05 28 11 83 02 41 dc 71 02 15 d0 38 73 02 15 e4
// f1 8c 05 84 60 a3 03 9a 50 21 8c 7b dc 01 24 a4
// f7 30 87 0a 73 b4 70 0f 27 18 08 83 81 0f 28 62
// 09 24 44 9d c5 09 86 d0 02 17 0f 04 d1 8e 03 94
// 00 01 13 d0 10 5b a7 22 43 41 04 ae 20 30 22 13
// 24 48 41 05 01 04 d0 04 16 08 ac 00 8d 6a f6 50
// 26 98 4e d1 03 04 64 3a cc 83 30 c2 51 83 1f 4c
// a0 49 82 49 85 c0 85 bb 8c 20 43 50 79 f4 08 86
// 9b fa 50 06 19 ee f8 22 10 23 b0 41 43 46 8a 52
// 40 13 28 12 99 a4 9d 9e 7e 17 09 34 f8 c2 85 7e
// 14 e1 76 23 e6 e0 28 c5 5c ec e0 eb 93 1f 10 86
// 02 2b 48 41 0c 34 a8 38 73 2f 74 b0 c2 9c 61 76
// 09 09 2c 92 56 03 00 40` }
