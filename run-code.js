/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires  */

// https://faucet.bitcoincloud.net/
// const PHRASE = 'rice return unknown unaware noble spawn bless satoshi boss swamp sunny ski';
// privateKey: cTxJE7H9pzXGCSUcBwfCY3ncyApnRfuESKqUpBycBvopXNuvZYZi
// publicKey: 02fb6504c2f2dcc67d596505d794a4d986fc89fa43af4a926039afa4f89a736220
// address: 1A2pseoNkhf5hjMC7Ezq4Dp1fG4CigtNFc
// 1NCXppQKhw4Mt5EeGQsX7bN8jLuR4ed4j7
// 15wwrHiWiepwf1xb4YjfVAvKchcKDG6gmJ


// run purse key 1: 'KyyhYRJs1rHs55VLT77KUZd9NQDk1qbYe3QbhfHUr6kitwMEj3DD'
// run purse key 2: 'L4JzeaJuCxP1RfxnGfcj3cVUA7epMYMBXSuTqbwPyCX9SqoRViUm'
// run owner address: '1JCFPC46Z4tvUoB9pn4Mp9a7y2V9XFjAvr'
// run owner key: 'L4QCb6eRKDsTBHyfZLQsAJwhGyC2b6oPYJbiM3QvkQe2aMBkkCnH

// loaded purse key: KyyhYRJs1rHs55VLT77KUZd9NQDk1qbYe3QbhfHUr6kitwMEj3DD
// other purse key: L4JzeaJuCxP1RfxnGfcj3cVUA7epMYMBXSuTqbwPyCX9SqoRViUm


// owner test key: cTc7xwzzBYPP2BBByA5ypNdQY3XgbXiJgh9MrHjkhEUpjW6gjYb4
// purse test key: cTxJE7H9pzXGCSUcBwfCY3ncyApnRfuESKqUpBycBvopXNuvZYZi

class MediaItem extends Jig {
	static validation = {
		Item: {
			creationDate: 'toBeString',
			title: 'toBeString',
			type: 'toBeString',
			categories: 'toBeArray',
			description: 'toBeString',
			key: 'toBeString',
			validationMsg: 'toBeString',
			thumb: 'toBeObject',
			content: 'toBeArray',
			meta: 'toBeObject',
		},
		ItemRequired: [
			'creationDate',
			'createdBy',
			'title',
			'type',
			'description',
			'thumb',
			'content',
		],
		ItemValidation: {
			key: 'toBeString',
			msg: 'toBeString',
		},
		ItemContent: {
			title: 'toBeString',
			description: 'toBeString',
		},
		ItemContentRequired: [
			'title',
			'description',
		],
		Media: {
			tx: 'toBeString',
			encrypted: 'toBeBoolean',
		},
		MediaRequired: [
			'tx',
		],
	};

	init(ownerPubKey, props) {
		expect(caller).toBeInstanceOf(MediaDispenser);
		expect(ownerPubKey).toBeString();
		MediaItem.validateProps(props);
		this.data = props;
		this.owner = ownerPubKey;
	}

	send(to, key, validationMsg) {
		expect(key).toBeString('Key is not a string');
		expect(key.length).toBeGreaterThan(0, 'Empty key is invalid');
		expect(validationMsg).toBeString('Validation msg is not a string');
		expect(validationMsg.length).toBeGreaterThan(0, 'Empty validation msg string is invalid');

		// TODO get PubKey.fromString working w/ Run
		expect(to).toBeString('Recipient is not a string');
		expect(to.length).toBeGreaterThan(0, 'Recipient is invalid');

		this.owner = to;
		this.data.key = key;
		this.data.validationMsg = validationMsg;
	}

	static validateProps(props) {
		const {
			Item,
			ItemRequired,
			ItemContent,
			ItemContentRequired,
			Media,
			MediaRequired,
		} = MediaItem.validation;

		function entriesLoop(obj, fn) {
			Object
				.entries(obj)
				.forEach(fn);
		}

		function validateRequired(obj, requiredList) {
			return requiredList.every(key => expect(obj[key]).toBeDefined(`Missing required property "${key}".  Obj: ${JSON.stringify(obj)}`));
		}

		validateRequired(props, ItemRequired);

		entriesLoop(props, ([key, val]) => {
			expect(val)[Item[key]]?.(`Item validation failed: Key "${key}" / Val: "${val}"`);
		});

		const media = [
			props.thumb,
			...props.content.map(c => c.media),
		];

		media.forEach(m => {
			validateRequired(m, MediaRequired);

			entriesLoop(m, ([key, val]) => {
				if(key === 'mimeType') {
					expect(MIME_TYPES.includes(val)).toBe(true, `Media mime type not recognized: ${val}`);
				} else {
					expect(val)[Media[key]](`Media item validation failed: Key "${key}" / Val: ${val}`);
				}
			});
		});

		props
			.content
			.forEach(c => {
				entriesLoop(c, ([key, val]) => {
					if(key !== 'media') {
						validateRequired(c, ItemContentRequired);
						expect(val)[ItemContent[key]]('Content validation failed: ', key, val);
					}
				});
			});
	}
}

class MediaDispenser extends Jig {
	init(ownerPubKey, props) {
		const {
			maxBatches = 0,
			maxIssues = 0,
			mediaData,
		} = props;

		expect(maxBatches).toBeNumber();
		expect(maxIssues).toBeNumber();
		expect(ownerPubKey).toBeString();

		MediaItem.validateProps(mediaData);

		const {
			creationDate,
			createdBy,
			categories,
		} = mediaData;

		Object.assign(this, {
			owner: ownerPubKey,
			mediaData,
			data: {
				creationDate,
				batchRuns: [],
				mediaData,
				createdBy,
				categories,
			},
		});
	}

	send(to) {
		this.owner = to;
	}

	create(count) {
		expect(count).toBeNumber();
		expect(count).toBeGreaterThan(0);
		const data = this.data;

		if(data.maxBatches) {
			expect(data.batchRuns.length).toBeLessThan(data.maxBatches);
		}

		const totalIssued = data.batchRuns.reduce((sum, issued) => sum + issued, 0);

		if(data.maxIssues) {
			expect(totalIssued + count).toBeLessThan(data.maxIssues);
		}

		const batchNumber = data.batchRuns.length + 1;
		const newIssues = [];

		for(let x = 0; x < count; x++) {
			const batchItemNumber = x + 1;

			newIssues.push(new MediaItem(this.owner, {
				instantiatedBy: this.origin,
				batchNumber,
				itemNumber: totalIssued + batchItemNumber,
				batchItemNumber,
				...this.data.mediaData,
			}));
		}

		this.data.batchRuns.push(count);

		return newIssues;
	}
}

class Payment extends Jig {
	init(owner, satoshis) {
		expect(satoshis).toBeGreaterThan(0);
		expect(owner).toBeString();

		this.owner = owner;
		this.satoshis = satoshis;
	}

	withdraw() {
		this.satoshis = 0;
	}
}

class OfferManager extends Jig {
	init(pubKey) {
		expect(pubKey).toBeString();
		this.data = {
			pubKey,
			offers: {},
		};
	}

	createOffer(amount, mediaItem, listingPost = null) {
		const {
			offers,
			pubKey,
		} = this.data;

		expect(offers[mediaItem.origin]).not.toBeDefined();
		expect(mediaItem.owner).not.toEqual(this.owner);

		if(listingPost) {
			expect(listingPost).toBeInstanceOf(ListingPost);
		}

		const newOffer = new Offer({
			amount,
			pubKey,
			mediaItem,
			listingPost,
		});

		offers[mediaItem.origin] = newOffer;

		return newOffer;
	}

	removeDeclinedOffer(mediaItemOffer) {
		const {
			offers,
		} = this.data;

		const removedOffer = offers[mediaItemOffer] || null;
		delete offers[mediaItemOffer];

		return removedOffer;
	}
}

class Offer extends Jig {
	init(args) {
		const {
			amount,
			pubKey,
			mediaItem,
			listingPost =  null,
		} = args;
		expect(caller).toBeInstanceOf(OfferManager);
		expect(amount).toBeNumber();
		expect(mediaItem).toBeInstanceOf(MediaItem);

		if(listingPost) {
			expect(listingPost).toBeInstanceOf(ListingPost);
		}

		this.data = {
			amount,
			transaction: '',
			requesterPubKey: pubKey,
			listingPost,
			messages: [],
		};

		this.owner = mediaItem.owner;
	}

	accept(transaction, message = '') {
		expect(transaction).toBeString();
		expect(message).toBeString();

		this.data.transaction = transaction;

		this.send(this.data.requesterPubKey, message);
	}

	decline(message = '') {
		expect(message).toBeString();

		this.send(this.data.requesterPubKey, message);

	}

	send(to, message = '') {
		expect(message).toBeString();

		if(message) {
			this.data.messages.push({
				sender: this.owner,
				message,
			});
		}

		this.owner = to;
	}
}

class ListingManager extends Jig {
	init(pubKey) {
		this.data = {
			listings: [],
			pubKey,
		};
	}

	createListing(ownerPubKey, props) {
		const {
			message,
			price,
			mediaItem,
			boardOrigin,
		} = props;

		const newListing = new ListingPost(ownerPubKey, {
			message,
			price,
			mediaItem,
			boardOrigin,
			requesterPubKey: this.data.pubKey,
		});

		this.data.listings.push(newListing);

	}

	sendTo(to) {
		this.owner = to;
	}

	removeListingPost(listingPost) {
		expect(listingPost).toBeInstanceOf(ListingPost);
		this.data.listings = this.data.listings.filter(l => l.origin !== listingPost.origin);
		listingPost.data.removalHandle.destroy();
	}
}

class ListingPost extends Jig {
	init(ownerPubKey, props) {
		const {
			message,
			price,
			mediaItem,
			boardOrigin,
			requesterPubKey,
		} = props;

		expect(caller).toBeInstanceOf(ListingManager);
		expect(message).toBeString();
		expect(price).toBeNumber();
		expect(boardOrigin).toBeString();
		expect(requesterPubKey).toBeString();
		expect(mediaItem).toBeInstanceOf(MediaItem);

		this.data = {
			message,
			price,
			boardOrigin,
			mediaItem,
			requesterPubKey,
			approved: false,
			removalHandle: new ListingPostRemovalHandle(this),
		};

		this.owner = ownerPubKey;
	}

	setApproved(approved) {
		this.data.approved = approved;
	}

	postTo(to) {
		this.owner = to;
	}
}

class ListingPostRemovalHandle extends Jig {
	init(post) {
		expect(caller).toBeInstanceOf(ListingPost);
		expect(post).toBeInstanceOf(ListingPost);

		this.data = {
			post,
		};
	}
}

class PublicListingBoard extends Jig {
	init(props) {
		const {
			name,
			description,
			categories,
		} = props;
		expect(name).toBeString();
		expect(name.length).toBeGreaterThan(3);
		expect(description).toBeString();
		expect(categories).toBeArray();

		this.data = {
			name,
			description,
			isModerated: true,
			categories,
			approvedListings: [],
		};
	}

	setModeration(isModerated) {
		this.data.isModerated = isModerated;
	}

	addListing(listing) {
		expect(listing).toBeInstanceOf(ListingPost);

		if(listing.approved) {
			return;
		}

		if(this.data.approvedListings.some(l => l.origin === listing.origin)) {
			return;
		}

		listing.setApproved(true);
		this.data.approvedListings.push(listing);
	}

	removeListing(listing) {
		expect(listing).toBeInstanceOf(ListingPost);
		this.data.approvedListings = this.data.approvedListings.filter(l => listing.origin !== l.origin);
		listing.setApproved(false);
		listing.destroy();
	}

	setName(name) {
		this.data.name = name;
	}

	setDescription(description) {
		this.data.description = description;
	}
}

MediaDispenser.metadata = {
	author: 'DavidDoesStuff',
	emoji: '📖',
	description: 'Creates and tracks media items.',
	image: await Run.extra.B.load('5de5ca0a3a67ffa2ce87a6cf242df668a6137be63426ea655793cd934a39f659'),
};

MediaDispenser.deps = {
	MediaItem,
	expect: Run.extra.expect,
};


MediaItem.metadata = {
	author: 'DavidDoesStuff',
	emoji: '📖',
	description: 'Media collection with public and private content.',
	image: await Run.extra.B.load('5de5ca0a3a67ffa2ce87a6cf242df668a6137be63426ea655793cd934a39f659'),
};

MediaItem.deps = {
	MediaDispenser,
	expect: Run.extra.expect,
	MIME_TYPES: [
		'application/epub+zip',
		'application/pdf',
		'audio/aac',
		'audio/mpeg',
		'audio/ogg',
		'audio/wav',
		'audio/webm',
		'image/bmp',
		'image/gif',
		'image/jpeg',
		'image/png',
		'image/webp',
		'video/mpeg',
		'video/ogg',
		'video/webm',
	],
};

Payment.deps = {
	expect: Run.extra.expect,
};

Offer.deps = {
	MediaItem,
	OfferManager,
	ListingPost,
	expect: Run.extra.expect,
};

OfferManager.deps = {
	Offer,
	ListingPost,
	expect: Run.extra.expect,
};

ListingManager.deps = {
	ListingPost,
	expect: Run.extra.expect,
};

ListingPost.deps = {
	ListingManager,
	MediaItem,
	ListingPostRemovalHandle,
	expect: Run.extra.expect,
};

ListingPostRemovalHandle.deps = {
	ListingPost,
	expect: Run.extra.expect,
};

PublicListingBoard.deps = {
	ListingPost,
	expect: Run.extra.expect,
};

const run = store._run;

run.deploy(MediaItem);
run.deploy(MediaDispenser);
run.deploy(Payment);
run.deploy(Offer);
run.deploy(OfferManager);
run.deploy(ListingManager);
run.deploy(ListingPost);
run.deploy(ListingPostRemovalHandle);
run.deploy(PublicListingBoard);

await run.sync();

console.log({
	MediaItem: MediaItem.location,
	MediaDispenser: MediaDispenser.location,
	Payment: Payment.location,
	Offer: Offer.location,
	OfferManager: OfferManager.location,
	ListingManager: ListingManager.location,
	ListingPost: ListingPost.location,
	ListingPostRemovalHandle: ListingPostRemovalHandle.location,
	PublicListingBoard: PublicListingBoard.location,
});
