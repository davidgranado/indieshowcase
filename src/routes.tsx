import { AuthGuard } from '@components/auth.guard';
import { RunGuard } from '@components/run.guard';
import { IonRouterOutlet } from '@ionic/react';
import React, { lazy, Suspense, useEffect } from 'react';
import { Route, useLocation } from 'react-router-dom';

const HomePage = lazy(() => import('@pages/home.page'));
const EncodeFilePage = lazy(() => import('@pages/encode-file.page'));
const ItemPage = lazy(() => import('@pages/item.page'));
const ProfilePage = lazy(() => import('@pages/profile.page'));
const Logout = lazy(() => import('@components/logout'));
const LoginPage = lazy(() => import('@pages/login.page'));
const RegistrationPage = lazy(() => import('@pages/registration.page'));
const RecoveryPage = lazy(() => import('@pages/recovery.page'));
const ListingsPage = lazy(() => import('@pages/listings.page'));
const MarketPage = lazy(() => import('@pages/market.page'));

const FULL_SCREEN_ROUTES = [
	'/getting-started',
	'/login',
	'/register',
	'/account-recovery',
];

interface Props {
	onPathChange?: (paneDisabled: boolean) => void;
}

export
function Routes(props: Props): JSX.Element {
	const {pathname} = useLocation();
	const {
		onPathChange = () => null,
	} = props;

	useEffect(() => {
		onPathChange(FULL_SCREEN_ROUTES.includes(pathname));
	}, [pathname]);

	return (
		<IonRouterOutlet id="main">
			<Suspense fallback={<div>Loading...</div>}>
				<Route
					exact
					path="/"
					component={() => (
						<AuthGuard>
							<HomePage/>
						</AuthGuard>
					)}
				/>
				<Route
					path="/profile"
					component={() => (
						<AuthGuard>
							<ProfilePage/>
						</AuthGuard>
					)}
				/>
				<Route
					path="/account-recovery"
					component={RecoveryPage}
				/>
				<Route
					path="/market"
					component={MarketPage}
				/>
				<Route
					path="/listings"
					component={ListingsPage}
				/>
				<Route
					path="/logout"
					component={Logout}
				/>
				<Route
					path="/encode-file"
					component={() => (
						<AuthGuard>
							<EncodeFilePage/>
						</AuthGuard>
					)}
				/>
				<Route
					path="/login"
					component={() => (
						<AuthGuard to="/" negate>
							<LoginPage/>
						</AuthGuard>
					)}
				/>
				<Route
					path="/register"
					component={() => (
						<AuthGuard  to="/" negate>
							<RegistrationPage/>
						</AuthGuard>
					)}
				/>
				<Route
					path="/item/:txId"
					component={() => (
						<RunGuard>
							<ItemPage/>
						</RunGuard>
					)}
				/>
			</Suspense>
		</IonRouterOutlet>
	);
}
