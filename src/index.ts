import { createElement } from 'react';
import { render } from 'react-dom';
import { App } from './app';
import { firebaseConfig } from '@common/constants';
import { initializeApp } from '@common/fb';

initializeApp(firebaseConfig);

render(
	createElement(App),
	document.getElementById('app'),
);
