declare module 'moneybutton' {
	export
	const moneybutton: {
		IMB: any;
		render(): any;
	};
}

declare module 'bsv' {
	export
	const bsv: any;

	export
	const Bip39: any;

	export
	const Bip32: any;

	export
	const PubKey: any;

	export
	const PrivKey: any;

	export
	const Ecies: any;
}

declare module 'run-sdk' {
	import { Jig } from '@common/jigs';
	import { Transaction } from '@common/jigs';
	import { Class } from '@common/types';


	interface Wallet {
		privkey: string;
		address: string;
		bsvAddress: any;
		bsvPrivateKey: any;
		bsvPublicKey: any;
	}

	interface Purse extends Wallet {
		Ha: number;
		Ka: number;
		blockchain: any;
		script: string;
	}

	interface Owner extends Wallet {
		pubkey: string;
	}

	interface LoadOptions {
		truse: boolean;
	}

	interface Extra {
		B: any;
		expect(arg: any): any;
	}

	interface Config {
		owner?: string;
		purse?: string | PayServer;
		network?: 'main' | 'test' | 'stn' | 'mock';
		api?: 'run' | 'mattercloud' | 'whatsonchain';
		app?: string;
		blockchain?: any;
		cache?: any;
		client?: boolean;
		logger?: any;
		netowrkTimeout?: number;
		networkRetries?: number;
		timeout?: number;
		trust?: string | string[] | '*';
		wallet?: any;
	}

	interface Inventory {
		jigs: Jig[];

		sync(): Promise<void>;
	}

	type RunEvent = 'load' | 'sync' | 'publish' | 'update';

	interface PayServer {
		// eslint-disable-next-line @typescript-eslint/no-misused-new
		new(apiKey: string): PayServer;
	}

	interface Plugins {
		PayServer: PayServer;
	}

	export default
	class Run {
		static extra: Extra;
		static plugins: Plugins;
		static instance: Run;
		static Transaction: Class<Transaction>
		constructor(args?: Config);

		purse: Purse;
		owner: Owner;
		app: string;
		inventory: Inventory;

		activate(): void;
		deploy(code: any): Promise<void>;
		import(rawtx: string, options?: LoadOptions): Promise<any>;
		load(location: string, loadOptions?: LoadOptions): Promise<any>;
		off(eventName: RunEvent, fn: () => void): void;
		on(eventName: RunEvent, fn: () => void): void;
		sync(): Promise<void>;
		transaction(f: any): any;
		trust(tx: string): void;
	}
}
