import Run from 'run-sdk';
import { JigLocations } from './constants';
import { User } from './fb';
import { JigMap } from './jigs';

export
type Class<T = any> = new (...args: any[]) => T;

export
enum JigTypes {
	ListingManager = 'ListingManager',
	ListingPost = 'ListingPost',
	ListingPostRemovalHandle = 'ListingPostRemovalHandle',
	MediaDispenser = 'MediaDispenser',
	MediaItem = 'MediaItem',
	Offer = 'Offer',
	OfferManager = 'OfferManager',
	OfferRemovalHandle = 'OfferRemovalHandle',
	Payment = 'Payment',
	PublicListingBoard = 'PublicListingBoard',
}

export
enum AuthState {
	Unknown = 'Unknown',
	LoggedIn = 'LoggedIn',
	LoggedOut = 'LoggedOut',
}

export
enum Constants {
	OWNER_PRIVATE_KEY = 'OWNER_PRIVATE_KEY',
	ENCRYPTED_OWNER_SEED = 'ENCRYPTED_OWNER_SEED',
	RATE_KEY = 'RATE_KEY',
	BITCOIN_FILES_URL = 'https://media.bitcoinfiles.org/',
	VALIDATION_MSG = 'ok',
}

export
interface Media {
	tx: string;
	mimeType: MimeType;
	encrypted?: boolean,
}

export
interface Content {
	title: string;
	description?: string;
	media?: Media;
}

export
interface ItemInit {
	title: string;
	description: string;
	content: Content[];
	thumb: Media,
	key: string;
	validationMsg: string;
	creationDate: string;
	createdBy?: string;
	meta: {
		[key: string]: string;
	}

}

export
interface Item extends ItemInit {
	batchNumber: number;
	itemNumber: number;
	batchItemNumber: number;
	instantiatedBy: string;
}

export
type JigType = keyof typeof JigLocations;

export
interface Store {
	_jigs: JigMap;
	_run: Run | null;
	_anonRun: Run | null;
	loggedInState: AuthState;
	inventoryLoading?: boolean;
	rate: number;
	user: User | null;
	seed: string;
	keysInitialized?: boolean;
}

export
interface Config {
	FIREBASE_API_KEY: string;
	FIREBASE_APP_ID: string;
	FIREBASE_AUTH_DOMAIN: string;
	FIREBASE_PROJECT_ID: string;
	NODE_ENV: 'development' | ' production';
}

export
interface FirebaseWalletDoc {
	ecnryptedOwnerPrivKey: string;
	ecnryptedOwnerSeed: string;
	ownerPubKey: string;
}
