
import fb from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const {
	firestore,
	auth,
	initializeApp,
} = fb;

export
type User = fb.User

export
type Firestore = fb.firestore.Firestore;

export
type Auth = fb.auth.Auth;

export
type DocReference = fb.firestore.DocumentReference<fb.firestore.DocumentData>;

export
type CollectionReference = fb.firestore.Query<fb.firestore.DocumentData>;

export {
	firestore,
	auth,
	initializeApp,
};
