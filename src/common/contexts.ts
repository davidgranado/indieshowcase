import { useAppState } from '@/useAppState';
import { createContext } from 'react';
import { DefaultState } from './constants';
import { Store } from './types';

type ComputedState = ReturnType<typeof useAppState>[1];

type AppActions = ReturnType<typeof useAppState>[2];

export
const StoreCtx = createContext<Store>(DefaultState);

export
const ComputedCtx = createContext<ComputedState>({} as any);

export
const ActionsCtx = createContext<AppActions>({} as any);
