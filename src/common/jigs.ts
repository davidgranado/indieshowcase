// import Run from 'run-sdk';
import { Class } from './types';

// Start Run Code

// const Run: any = {};
// const caller: any = {};
// function expect(arg: any): any {};

interface ExportOpts {
	pay?: boolean;
	sign?: boolean;
}

export
class Transaction {
	update(fn: () => void) {
		console.error(fn);
	}

	import(tx: string) {
		console.error(tx);
	}

	async export(opts?: ExportOpts) {
		console.error(opts);
		return 'null' as string;
	}

	async publish(): Promise<void> {}
}

interface SyncOptions {
	forward?: boolean;
	inner?: boolean;
}

export
class Jig {
	static deps: any;
	static metadata: {
		author?: string;
		emoji?: string;
		description?: string;
		image?: any;
	};

	location!: string;
	origin!: string;
	owner!: string;
	purse!: string;
	satoshis!: number;

	async sync(options?: SyncOptions): Promise<void> {
		// Dummy code
		console.error(options);
		// eslint-disable-next-line no-debugger
		debugger;
	}
	async destroy(): Promise<void> {}
	constructor(...args: any[]) {
		// Dummy code
		console.error(args);
		// eslint-disable-next-line no-debugger
		debugger;
	}
	// abstract send(to: string, key: string, validationMsg: string): void;
}

const MIME_TYPES = [
	'application/epub+zip',
	'application/pdf',
	'audio/aac',
	'audio/mpeg',
	'audio/ogg',
	'audio/wav',
	'audio/webm',
	'image/bmp',
	'image/gif',
	'image/jpeg',
	'image/png',
	'image/webp',
	'video/mpeg',
	'video/ogg',
	'video/webm',
] as const;

type MimeType = typeof MIME_TYPES[number];

interface Media {
	tx: string;
	mimeType: MimeType;
	encrypted?: boolean;
}

interface MediaItemContent {
	title: string;
	description: string;
	media: Media
}

interface MediaItemValidatedData {
	creationDate: string;
	createdBy: string;
	title: string;
	type: string;
	categories?: string[];
	description: string;
	key: string;
	validationMsg: string;
	thumb: Media;
	content: MediaItemContent[];
	meta?: any;
}

interface MediaItemData extends MediaItemValidatedData {
	instantiatedBy: string;
	batchNumber: number;
	itemNumber: number;
	batchItemNumber: number;
}

class MediaItem extends Jig {
	data!: MediaItemData;

	static validation = {
		Item: {
			createdBy: 'toBeString',
			creationDate: 'toBeString',
			title: 'toBeString',
			type: 'toBeString',
			categories: 'toBeArray',
			description: 'toBeString',
			key: 'toBeString',
			validationMsg: 'toBeString',
			instantiatedBy: 'toBeString',
			batchNumber: 'toBeNumber',
			itemNumber: 'toBeNumber',
			batchItemNumber: 'toBeNumber',
			thumb: 'toBeObject',
			content: 'toBeArray',
			meta: 'toBeObject',
		},
		ItemRequired: [
			'creationDate',
			'createdBy',
			'title',
			'type',
			'description',
			'thumb',
			'content',
		],
		ItemValidation: {
			key: 'toBeString',
			msg: 'toBeString',
		},
		ItemContent: {
			title: 'toBeString',
			description: 'toBeString',
		},
		ItemContentRequired: [
			'title',
			'description',
		],
		Media: {
			tx: 'toBeString',
			encrypted: 'toBeBoolean',
		},
		MediaRequired: [
			'tx',
		],
	};

	init(ownerPubKey: string, data: MediaItemData) {
		expect(ownerPubKey).toBeString();
		expect(caller).toBeInstanceOf(MediaDispenser);
		MediaItem.validateProps(data);
		this.data = data;
		this.owner = ownerPubKey;
	}

	send(to: string, key: string, validationMsg: string) {
		expect(key).toBeString('Key is not a string');
		expect(key.length).toBeGreaterThan(0, 'Empty key is invalid');
		expect(validationMsg).toBeString('Validation msg is not a string');
		expect(validationMsg.length).toBeGreaterThan(0, 'Empty validation msg string is invalid');

		// TODO get PubKey.fromString working w/ Run
		expect(to).toBeString('Recipient is not a string');
		expect(to.length).toBeGreaterThan(0, 'Recipient is invalid');

		this.owner = to;
		this.data.key = key;
		this.data.validationMsg = validationMsg;
	}

	static validateProps(props: MediaItemValidatedData) {
		const {
			Item,
			ItemRequired,
			ItemContent,
			ItemContentRequired,
			Media,
			MediaRequired,
		} = MediaItem.validation;

		function entriesLoop<T, K extends keyof T>(obj: T, fn: (args: [keyof T, T[K]]) => void) {
			Object
				.entries(obj)
				.forEach(fn as any);
		}

		function validateRequired(obj: any, requiredList: string[]) {
			return requiredList.every(key => expect(obj[key]).toBeDefined(`Missing required property "${key}".  Obj: ${JSON.stringify(obj)}`));
		}

		validateRequired(props, ItemRequired);

		entriesLoop(props, ([key, val]) => {
			expect(val)[Item[key]]?.(`Item validation failed: Key "${key}" / Val: "${val}"`);
		});

		const media = [
			props.thumb,
			...props.content.map(c => c.media),
		];

		media.forEach(m => {
			validateRequired(m, MediaRequired);

			entriesLoop(m, ([key, val]) => {
				if(key === 'mimeType') {
					expect(MIME_TYPES.includes(val as any))
						.toBe(true, `Media mime type not recognized: ${val}`);
				} else {
					expect(val)[Media[key]](`Media item validation failed: Key "${key}" / Val: ${val}`);
				}
			});
		});

		props
			.content
			.forEach(c => {
				entriesLoop(c, ([key, val]) => {
					if(key !== 'media') {
						validateRequired(c, ItemContentRequired);
						expect(val)[ItemContent[key]]('Content validation failed: ', key, val);
					}
				});
			});
	}
}

interface MediaDispenserData {
	maxBatches?: number;
	maxIssues?: number;
	mediaData: MediaItemValidatedData;
}

interface MediaDispenserInternalData extends MediaDispenserData {
	batchRuns: number[];
	creationDate: string;
	createdBy: string;
	categories: any[];
}

class MediaDispenser extends Jig {
	data!: MediaDispenserInternalData;

	init(ownerPubKey: string, data: MediaDispenserData) {
		const {
			maxBatches = 0,
			maxIssues = 0,
			mediaData,
		} = data;

		expect(maxBatches).toBeNumber();
		expect(maxIssues).toBeNumber();
		expect(ownerPubKey).toBeString();

		MediaItem.validateProps(mediaData);

		const {
			creationDate,
			createdBy,
			categories = [],
		} = mediaData;

		this.owner = ownerPubKey;
		this.data = {
			maxIssues,
			maxBatches,
			mediaData,
			creationDate,
			batchRuns: [],
			createdBy,
			categories,
		};
	}

	send(to: string) {
		this.owner = to;
	}

	create(count: number) {
		expect(count).toBeNumber();
		expect(count).toBeGreaterThan(0);
		const data = this.data;

		if(data.maxBatches) {
			expect(data.batchRuns.length).toBeLessThan(data.maxBatches);
		}

		const totalIssued = data.batchRuns.reduce((sum: number, issued: number) => sum + issued, 0);

		if(data.maxIssues) {
			expect(totalIssued + count).toBeLessThan(data.maxIssues);
		}

		const batchNumber = data.batchRuns.length + 1;
		const newIssues = [];

		for(let x = 0; x < count; x++) {
			const batchItemNumber = x + 1;

			newIssues.push(new MediaItem(this.owner, {
				instantiatedBy: this.origin,
				batchNumber,
				itemNumber: totalIssued + batchItemNumber,
				batchItemNumber,
				...this.data.mediaData,
			}));
		}

		this.data.batchRuns.push(count);

		return newIssues;
	}
}

class Payment extends Jig {
	data!: any;

	init(owner: string, satoshis: number) {
		expect(satoshis).toBeGreaterThan(0);
		expect(owner).toBeString();

		this.owner = owner;
		this.satoshis = satoshis;
	}

	withdraw() {
		this.satoshis = 0;
	}
}

interface OfferMap {
	[key: string]: Offer;
}

interface OfferManagerData {
	pubKey: string;
	offers: OfferMap;
}

class OfferManager extends Jig {
	data!: OfferManagerData;


	init(pubKey: string) {
		expect(pubKey).toBeString();
		this.data = {
			pubKey,
			offers: {} as OfferMap,
		};
	}

	createOffer(amount: number, mediaItem: MediaItem, listingPost: ListingPost | null = null) {
		const {
			offers,
			pubKey,
		} = this.data;

		expect(offers[mediaItem.origin]).not.toBeDefined();
		expect(mediaItem.owner).not.toEqual(this.owner);

		if(listingPost) {
			expect(listingPost).toBeInstanceOf(ListingPost);
		}

		const newOffer = new Offer({
			amount,
			pubKey,
			mediaItem,
			listingPost,
		});

		offers[mediaItem.origin] = newOffer;

		return newOffer;
	}

	rescindOffer(offerOrigin: string) {
		const {
			offers,
		} = this.data;

		const removedOffer = offers[offerOrigin] || null;

		if(removedOffer) {
			delete offers[offerOrigin];
			return removedOffer.data.removalHandle.destroy();
		}

		return null;
	}

	removeDeclinedOffer(offerOrigin: string) {
		const {
			offers,
		} = this.data;

		const removedOffer = offers[offerOrigin] || null;
		delete offers[offerOrigin];

		return removedOffer;
	}
}

interface OfferMessage {
	sender: string;
	message: string;
}

interface OfferDataArgs {
	amount: number;
	pubKey: string;
	mediaItem: MediaItem;
	listingPost?: ListingPost | null;
}

interface OfferData {
	amount: number;
	transaction: string;
	requesterPubKey: string;
	mediaItem: MediaItem;
	listingPost: ListingPost | null;
	messages: OfferMessage[];
	removalHandle: OfferRemovalHandle;
}

class Offer extends Jig {
	data!: OfferData;

	init(args: OfferDataArgs) {
		const {
			amount,
			pubKey,
			mediaItem,
			listingPost =  null,
		} = args;
		expect(caller).toBeInstanceOf(OfferManager);
		expect(amount).toBeNumber();
		expect(mediaItem).toBeInstanceOf(MediaItem);

		if(listingPost) {
			expect(listingPost).toBeInstanceOf(ListingPost);
		}

		this.data = {
			amount,
			transaction: '',
			requesterPubKey: pubKey,
			listingPost,
			mediaItem,
			messages: [],
			removalHandle: new OfferRemovalHandle(this),
		};

		this.owner = mediaItem.owner;
	}

	accept(transaction: string, message = '') {
		expect(transaction).toBeString();
		expect(message).toBeString();

		this.data.transaction = transaction;

		this.send(this.data.requesterPubKey, message);
	}

	decline(message = '') {
		expect(message).toBeString();

		this.send(this.data.requesterPubKey, message);

	}

	send(to: string, message = '') {
		expect(message).toBeString();

		if(message) {
			this.data.messages.push({
				sender: this.owner,
				message,
			});
		}

		this.owner = to;
	}
}

interface ListingManagerData {
	listings: ListingPost[];
	pubKey: string;
}

class ListingManager extends Jig {
	data!: ListingManagerData;

	init(pubKey: string) {
		this.data = {
			listings: [],
			pubKey,
		};
	}

	createListing(ownerPubKey: string, listingProps: Omit<ListingPostDataProp, 'requesterPubKey'>) {
		const {
			message,
			price,
			mediaItem,
			boardOrigin,
		} = listingProps;

		const newListing = new ListingPost(ownerPubKey, {
			message,
			price,
			mediaItem,
			boardOrigin,
			requesterPubKey: this.data.pubKey,
		});

		this.data.listings.push(newListing);

	}

	sendTo(to: string) {
		this.owner = to;
	}

	removeListingPost(listingPost: ListingPost) {
		expect(listingPost).toBeInstanceOf(ListingPost);
		this.data.listings = this.data.listings.filter(l => l.origin !== listingPost.origin);
		listingPost.data.removalHandle.destroy();
	}
}

interface ListingPostDataProp {
	message: string;
	price: number;
	mediaItem: MediaItem;
	boardOrigin: string;
	requesterPubKey: string;
}

interface ListingPostData extends ListingPostDataProp {
	approved: boolean;
	removalHandle: ListingPostRemovalHandle;
}

class ListingPost extends Jig {
	data!: ListingPostData;

	init(ownerPubKey: string, props: ListingPostDataProp) {
		const {
			message,
			price,
			mediaItem,
			boardOrigin,
			requesterPubKey,
		} = props;

		expect(caller).toBeInstanceOf(ListingManager);
		expect(message).toBeString();
		expect(price).toBeNumber();
		expect(boardOrigin).toBeString();
		expect(requesterPubKey).toBeString();
		expect(mediaItem).toBeInstanceOf(MediaItem);

		this.data = {
			message,
			price,
			boardOrigin,
			mediaItem,
			requesterPubKey,
			approved: false,
			removalHandle: new ListingPostRemovalHandle(this),
		};

		this.owner = ownerPubKey;
	}

	setApproved(approved: boolean) {
		this.data.approved = approved;
	}

	postTo(to: string) {
		this.owner = to;
	}
}

class OfferRemovalHandle extends Jig {
	data!: {
		post: Offer;
	};

	init(post: Offer) {
		expect(caller).toBeInstanceOf(Offer);
		expect(post).toBeInstanceOf(Offer);

		this.data = {
			post,
		};
	}
}

class ListingPostRemovalHandle extends Jig {
	data!: {
		post: ListingPost;
	};

	init(post: ListingPost) {
		expect(caller).toBeInstanceOf(ListingPost);
		expect(post).toBeInstanceOf(ListingPost);

		this.data = {
			post,
		};
	}
}

interface PublicListingBoardData {
	name: string;
	description: string;
	isModerated: boolean;
	categories: any[];
	approvedListings: ListingPost[];
}

class PublicListingBoard extends Jig {
	data!: PublicListingBoardData;

	init(props: PublicListingBoardData) {
		const {
			name,
			description,
			categories,
		} = props;
		expect(name).toBeString();
		expect(name.length).toBeGreaterThan(3);
		expect(description).toBeString();
		expect(categories).toBeArray();

		this.data = {
			name,
			description,
			isModerated: true,
			categories,
			approvedListings: [],
		};
	}

	setModeration(isModerated: boolean) {
		this.data.isModerated = isModerated;
	}

	addListing(listing: ListingPost) {
		expect(listing).toBeInstanceOf(ListingPost);

		if(listing.data.approved) {
			return;
		}

		if(this.data.approvedListings.some(l => l.origin === listing.origin)) {
			return;
		}

		listing.setApproved(true);
		this.data.approvedListings.push(listing);
	}

	removeListing(listing: ListingPost) {
		expect(listing).toBeInstanceOf(ListingPost);
		this.data.approvedListings = this.data.approvedListings.filter(l => listing.origin !== l.origin);
		listing.setApproved(false);
		return listing.destroy();
	}

	setName(name: string) {
		this.data.name = name;
	}

	setDescription(description: string) {
		this.data.description = description;
	}
}

// MediaDispenser.metadata = {
// 	author: 'DavidDoesStuff',
// 	emoji: '📖',
// 	description: 'Creates and tracks media items.',
// 	image: await Run.extra.B.load('5de5ca0a3a67ffa2ce87a6cf242df668a6137be63426ea655793cd934a39f659'),
// };

// MediaDispenser.deps = {
// 	MediaItem,
// 	expect: Run.extra.expect,
// };


// MediaItem.metadata = {
// 	author: 'DavidDoesStuff',
// 	emoji: '📖',
// 	description: 'Media collection with public and private content.',
// 	image: await Run.extra.B.load('5de5ca0a3a67ffa2ce87a6cf242df668a6137be63426ea655793cd934a39f659'),
// };

// MediaItem.deps = {
// 	MediaDispenser,
// 	MIME_TYPES,
// 	expect: Run.extra.expect,
// };

// Payment.deps = {
// 	expect: Run.extra.expect,
// };

// Offer.deps = {
// 	MediaItem,
// 	OfferManager,
// 	ListingPost,
// 	OfferRemovalHandle,
// 	expect: Run.extra.expect,
// };

// OfferRemovalHandle.deps = {
// 	Offer,
// 	expect: Run.extra.expect,
// };

// OfferManager.deps = {
// 	Offer,
// 	ListingPost,
// 	expect: Run.extra.expect,
// };

// ListingManager.deps = {
// 	ListingPost,
// 	expect: Run.extra.expect,
// };

// ListingPost.deps = {
// 	ListingManager,
// 	MediaItem,
// 	ListingPostRemovalHandle,
// 	expect: Run.extra.expect,
// };

// ListingPostRemovalHandle.deps = {
// 	ListingPost,
// 	expect: Run.extra.expect,
// };

// PublicListingBoard.deps = {
// 	ListingPost,
// 	expect: Run.extra.expect,
// };

// End Run Code

// https://www.typescriptlang.org/play?noImplicitAny=false&strictNullChecks=false&strictFunctionTypes=false&strictPropertyInitialization=false&strictBindCallApply=false&noImplicitThis=false&noImplicitReturns=false&alwaysStrict=false&esModuleInterop=false&experimentalDecorators=false&emitDecoratorMetadata=false&target=7#code/MYewdgzgLgBASgVzALhgQzATxgXhgbwF8BuAKFElmDQBsaBTAJ1Q2zyLIDMlgoBLcDHoAPAA71eACjSMA5iywBKBdg6lSUTOJgBhGmggQAPABVc6LAD5zYegHcYkgHQuZsiCoDaAXUW5rJmSkfGBQTJxowPQwAMqYYMAA8qL84BAEpACQnCCMdjIAJgD8qABGICAMGGSZIbaMJTDllfTVpITqaKXQjJFU+oYwAFJ8shmZ0Gj8wDAF9KIeFpg1k9MwALb0UGgFU2io+FmZaAhQABa5jT0hsjWZ9OsgAFZ8V1CMN3dzEMAfKQJgN4fMC3I58dZoWT0RqsGokdSZGggaipMAAQlQ1xBNVyoxCGJgWNBmRAdnqBKJNVECEYEHoFPenyyECmIAgZz4EAJYAQ61KTCCxwg8RmwoSkhA-zSjTiCWSqIgyhgAAVGCB1pz6EYAG4gPgFayHTKZAD0JpgABFeetsKA5kcKBAWk4kbIJVLIIo4VkDCLZvQeiBMJIlar1ZqdXqDQQOplHe8ELxcs5XHJFqwfH4jabzVb1jaYHb6A60s7XdI016srGzehuu8+oT6GACpIoCBMYyQQAaGAAa3omE7wNkve1tH1UwBAFkIPJCV3ZErdfqyB11PGYNOAJLTgCiAH0TABNZV7mLmTxZADkaFEohofBRAJN8wQpQA1AAvPiia-dm87wfJ8p3AE1RAKTh-0AhACgEE00EiaDMlvWD4PWcRZGQ1C4JAE0QFkLCAJQk5cJNfJtWw0j4LsehSnWbDwUhegTXov9iOvJioRNWQ+CgjiuJYp5MMYiFuNEEFROY8i6PYm9tX1eg8Iw+giPkxS8IItSUIUuY8No+joO8dB0njIJNG0adwXoEwtGiPALKUzgt13Q8TzPGJPB5PkmG8IIQjCRgIiiLd6DgtBxigYRhyZTINU2WzxFQKyErsmpm1+LQwmKMoKiqMA13UALwkiaJpzCvg0G3MJ1h0cAwlCSK+CgBgYuxLJvl+X9UTa4lNnC5KKrQdoitCEqQvK8LqoeAA1Cddmyi09nGX5WlRJawl6mpVqmMKACEhwXEcan4Fr6C2rJHIuuNdtkXEAyBG4fBqTq-h6o7YoHQ7KSycdHwWmc52u85eVKQbwu2+rmygcHKum2qodCZ6sk2bYYSwQrgjGoLStCqaao2iKRAagp0kmuGarm-7doKQnxhCSZQkq7KDuu0opmAM4ADlQaYVBvP5Rgamah4eZ85gYAFgUsnZqBOfhsXBf53mhZG8gBjJob4aEYQSfSEYxiNBa0AJcmqoJvZBVWJ8YD+ydUXMbN4YOI440YNb9sO692z2+gYkXZDjR29bdtQb2QF9-2R0DzJTtamBw8jgPiONK6E59v3k9dlFVPuxZE-oABBRhekwGPXu6gEw4zqObhjr7q4jzPo5TzI7YB8BZ3nAva8k1uGe2JmadZ9Om977TjVlznFb50ffZnxgY5F9YF8b+eVZjqezgVlW1-oBeY5Bvk98SUongkKAY4oBqYbnouS7QMvW9R-Y79P8-eEDwgU-huB6AARwQHwd2BRUBXmNChYOAINr0BjteYOns4Fx1ga3b2dk4EVw9Egs4oM4HX2hoHbwP9KbzVAigcYxoG533HjHdYQNqFZ2NN-I48M6pjVCC7CByC940Nbpg96PdGGZGYcaVhiMoB-0AcAsKYDXbe2agwDBAYupYJTkQo4ZtOGp2igwlursMqMCyjIu+e08qtDAF-FOZtJFAJAbIiB3thCEOIvCLIIRmoSjJEwZU74ADSg5eq9mNrDc2DxCZZiOCIcQUhST1B8aUfxmBFBOBrouEMdwokX0kNQOgTBkkZ23JQDAUREicEkGbC0nJxCQDyXcM28MnDt12mGBYkhjZVlThyCAThjbmGNncc4nInCxKYOYEZjB4mJO9MyZsrZ2yBP7AEj6PZbakNRF3XqETjSZKkF9fJY80nXkSTATkksQCwAikSa8HT7hiCyV9F0zZZDnH2b7AA4u7XajATBnAwJIAADL2a8e4MKaEWdgU5IR27XIyXcqQTT1lzlec3G4khrxU3tgCDYc4TnpDAOc9AyysI3J2VASQCLAayEeSCF5KSm4fI9t835YAAVApBSkbAFLBB0LGESXFJywDQqrEcWsJhEgWkSDAKEsBJmDicJwNU6xx4wDsLkPsNwVXmkQGASJcKyXtmRePNFf9gC-j4NDfl+KLlEphbq6J+qQDUueWcZFDKvk-L+YChOJqzUWshYK+atqjiDO6eM8w7YBldJ6XsJwX1zBfUjUM42jS1mUvMFysAXdpkTG2GsBF9AWkQEkKINUCwQnwwxQtMK4SKGbmzJkeGrdf4ANsWFJtNU2E33bQ8Tt0MbHSIKK3M2Q6hr9pASnQg5h6k1RTdTVEgpsg8AdtDD4AYAAyFRRCmF7L4nWetwUgGciYSwEoz6oBML2Tg5CKzuDAV9Q9MAL2Ps8L47wvh-C2yjFs4078L6u0yE4Fd5qi0gDPoof98rch7kiGcSQV6TJLBueuY03AEgO3zWOsKp6ngqF7O7KRIC12clvkSTMFDMjuygDSMAMB8OtoKER6AgHtRMGDHGnA1hSXYc8F9XwdLfYWnoJwEIWGAAGVlDAarowOmAJbJRMDBQAIgACT4C+oQRTTgYAwHfqgVTQwYiJC5k4IkfFgygaeIoQgonFBIYRG3NZ9BMOtjkwsXszaCNhWFds0Iq6IAbslMW0tEBeySB44OMctB30cfI1xv6ihPDw3C5gN9RQnCSFE9rDNMAIh8AYKAmAxyVNqcHBpmA5oqaoGK39DTNm7iEG83GNIsB+qVUvK7Vz3Sj6lFbi4JwnWnD4NCE4CEohskfuACNoaig1ELta2gSDjBoOc0kOsD99aMMtoHat3s1itsgMa-cXzwGAtjfWKF5LkWaDRcNP+vikh2M4DwNeeKNl0HfogVxnc+4jynnPE4EIwAaCwQDOS2gCHWC2f-cafj9A2yMAQPQXs4mhobGsjARyZzYDu1ALIMAfAvzGNUzVur-7J30BoHScj2y9Vg5oAls2yW30ZbNicmqqy51Yty-l1ARXVPqcU+VmAlWYDE9oNZm5TDXYNfq41zrrtBviIVzkJbMHxsxfrUdxk67N3ZIu19K7N3qe1DKXGtET2E7zeucbhzc6nP7aw8AdzHbxHOcl592n8XEsu-YVAJn3g0W9satl7nxj-zgqu+74RZPJcy+rGrYqOMJpDUqRAapdJGB0yNBCYQe0OZnAesrcWNQc-bkMIjiAjQpaqziinvY5aSF29ppbBP2NgplRT1U5sGfCmBTALQOmxNZma3Cqn9PTAs8y3z9qxY1fkZuzWtA0ORLtqfJZt9Rc21bp5y8H5NWQODAj8qmP7voyh8tn1qMcYxtTad7T6fxgvemD95oITQU7iyXjNlRvkcQT6942Py7xqUzz2G-TrVdhzzzzlgL3SDwEBQgLQGEDLwgAr3MHgIgXm0JgnT6RbztSyUgPzwDGRQXnSTwKkFL3LyINhxIJJVpy-z8UHENTSUa2nQeFnXtgLWC1Wzr22Ea3AIgSgXABgVbgQQKAOhEO3z80vHUSYXMEwNwM6SGTDTwHoISUHETW6V6XYAQKQMoJCx0KgM5gDGfh4LQBELXyXzCFbi3hnzARkKDjX09gkLCDuj8wnWzTpBbDbA7CJW-RDWGS8UYHDRAGzQQWyRACQFvmr2-S41AEiOIJVlIJp3tXCPiNhzdUCg9RZX+RuU3C0IxyjX6Xs3u2TQIOgKINi1p2TRsKQG6QYBpRdVhzXQDAgCyLaRjTKKMMVHq3s03HbG2BoGQMRwKBwO2CcBqMgCcBAUTDh0kBQPWCL0Fl7E5BQOMWiI-XmJgA-FxVWMHRgByIXRKI6MQKGIqPrS436NoFOJGO2LiNCGRWaMMDaNKJOL0LsxLEoCaHzwXlGIWwmLqKeXOC2JgAAEY7hNxbA7BTjYCYBkYjgVdJAGBYBhA0DiAYAUSjBCwIjQg0ThAPwPwPtNwt4d5xZzAUTtiwT7NMhIToT+sEB2RJBISADQl1g2wo1xlexNcB4MB+Bh5Dp-DcReIwBW5Mgt4F4RTl5V4MdzkrjKCbivjoCSSlj-1iSapxSIMXB-DSjTDW4Gt3jFDNCY1-i6SGS7ioBGtKNqNJZ7BoTvR1wD9BhlRH5NhGpz9SZhgr8jY9gCRYQEQP9PF6gFkWR2x2RORFi8kKEuNgy2QulXVHCmVPVaCUjxkmCRxSDg12TAixlAiNCTNWRQyYToyCzs07BmozgCheg7AQwKF-CiyulUT491xE928dNOBOAmBpw7xxhktepvBUBSl2zVYmy29cYByOyMBmIQDthxhqQ1Cf9YpD1BzFgxzGBOzRBMYHT0gVzOz+8oQgi3TL9DYOpvT+y2zxzdyJ9cC3F8cyVZzElNlIzac7zGDYcjUbktTlptCIFnzMBW5FymBFgiAENty7x3D48sgEEVzpBHhIiFl5tnZmTG0YBHxoAbhlQ2Rb5GN+AQR0LoAYAAAfSWBAOgGwYi+nWtZrcjf82kVuH87AhyQohQ25ZMs82kTweCmdQUkIPjK1WHQTYTWwVsJM-ArWTiwI5JXi9sPcQBWgNkpQ8Sw4spFC7C2QXC80yolI5StCjC5FQpRmEpMpLC7S6AfUprT4yElcmwewVswcyQetNAGC0IWihg38iA0Sh4VuLSnCjCidRraiiAdi9y9YYZD4IU4yPACy1ihdS0xgGjSKwc7Nd2R4FjQTIHETAoKC6ixIUKkIB87Mfg40fy+igopNJizcJKkAFjDK1isZVigKrKnKsAYyfCwinkOgL4CnLYaIfyzwBqvEJq6KrYK0iqqqlcu00aQKFs7clo5icYTwuYCWH6WvQwZiC6Ycya0c1iwmYudwcYBy7EqIlWKkFy66Dih4BvB4GoLy1SjCxoIy7yvC1qsizGZszawcyfY4Ryw64vS6XoSAPoKuFfLIOjAMQKb-a6a6tS1Ae6m6x6oi9qlGGaqEZc1i8qFaqEZGe0jWGys-XWYfD0o8zIG-U896hQ-0twFG0m7YHaxUCiz4+yr65yuckw-GDy12SGjC8weGmgYqim2FFInJBgRgXSopBIegUpSQECi84W-mrJfatIjOGg2W8goKkW-S8WspVg9YRre7DmkyjSrJPW802HPSweAyyQGGtS0yj86cr840eWpy12Bsf63gQG68GOEG6AbxE62TFyzy4jYyqAEw9GgMOwsCjM+S+oOQoKgI+obNSIKIFIeHDAFkV28AOCpG+yBOa3bMC4v61O1EVM1FYSlWkOoukEdMg06NMY52gurFByfOgG8ABdWs2ZOSw0sYz2sGv2jYTOjpZDOYNK2wbgkO8wd2mI2nTYMu185gluqNea9u6uhbLu725m3usuwUZDBe+ZIlXsKelkKEMenOsgslfe5icut0HWspM+qED7G2hbG+gME02DTXea2eAUwIkUx+3U62zMqOhyEIxsia8aaIGGncycj666xYS2jC+fH8tajcbGsBicvcvdfGg2a-E8mAZB6Wt-P0m84tH2okPwxi228jKBsOjrP2o4eEaPCCxwmGgM1e+83e5CgOnC4LfsjUKAIwGB6AQmFpIFFeiZFy68SwMAyi+tb+jrD4KIFmimNmiBcoQobK-q4qyG4LBdCE+wGGqyhwPhz-QI7-TktykO2i2RpHNy1m87V2ZRxgDKxq1uYR8GkqjuhbOimhxre+l0dh9wZ+xknR3x7zLe2ZEwEAbwvKiO0NLMgBxKh4Sq+gAxxE3xqG7BlJnSx8zS9Jkyk20W82gx980hhbKB8NIpnx1CkEbpYTGgQKREj9GgEK-qmAc3PAI2xpoUm5Np5NEa2gAACQwAKAYB6VBrVGDH7tbw2pCgMYEdLXGEfuuhLSfHOiIp+tr2sYuvWBqDsYcf6uumcaIc3wmZAbSYqdhqgEHzxovxOZUrUpmclD2vvDVCqtyhaDaAo3ib+n6ZbHjgMb-mSr6YGYYA3KQeydgAPIJswe2AJGmbJoIdUJYaJF7E62htBbudEAkfptMYPsse-IsfkZZOsJABUccddn2eZpofME6wXViNoCFrVrNo1ott8fAb3JLtPr7pnrTLZaCyWYSPFiSOYqyW2dUY6c5eLuVrJTJcSQvoFa4zOu1tyfVolq1q8aKcdixeYnMaWcJeJbUasYUZsYgSlYi1diAieeMQiEpxxeNB6ZoC+cGeWaZN+Y+YBe+bhxDRmxobnsjtGRUKMZco8K2ELkeYSdbDNdDZefyhIdKrGPDaqvMDjbCmzVEAwrCYid8JrL-t9elPGvVkP2ucDr+cqtdYdbQauYwa9Kha0RTegBRdObUrhHfwIZrcwtBYnoFtpbyUVYZYloKYleLR0u7eKUZb7fs3vvVe-J8q9aAaxkmeiHiUfGABhtMUKA+v702Gun4UBqWs5GnBAAWppkjfMS3xcJ3yWHn0TYY18egdBcxsQfzYXafGXaJfsbLfdIrePKrZVHfEXefdXdhY8WRe-dKF-d8ZXfsZrXyskddnXetaJuUTegBGcNzjcIpbwCpZPsZIcvoBle5dg6dVpQzgyKYDaIAGZuWt3wBcP+2c5XDgNkVi5S5K7Y41W7bqTsO+EEPK5wB+4IA92D3spUAEw4PaO85W5L2YbFgfAwK6GJgth+OmAyFJBd393FPBOmgzEMBo23GAc+PVPehspzAVOBOk3wLjgCgr3TnknTm62bmMnc7adrr6Xh3e223FLrOVKl6nBL2PsYqCoaHiiylvGJPr2TN1Q4caB6n2mQhcALdrrouwBbNyM-Oeijh4u6QoBg25Mqr4dEdCmY2FsQvTnulqQGSnO4nkrEnfGPObhbPA723Dagmh2xbXP62dLczk0iuVKYTguQ2qrJP5U8tanIuYt4uuKaMWnkKEvOnfGTMg2+usNLW6QZvTnhnAwxnA2oAuZsOsON2M3sxvHYObBsPNvBMfhEPwA2kuOPRImq7k1KOaM8AHvc2KkgCM8ptth8ijQThzhcgw4lpdILQQAAx-YEA2zkJ4mXgw5ABeDcADWd5CB7sOHQRw9IAZjHXoYAPsdIebNnB4bpZCQSFgfIZqeAJAQDXWXoJwPaF0Il1sa8AAVjmHp+oH+TQFI7QAADYAB2NstAAAJiiAAA4ufOfgBOA+eAAWPnyCDnjnwXznkE0jrn-kDn0jqXjn1oDn+n+nrngATlI+AAKH14l7Z9184C191+uQAlcVe-v2AOGYWAna1uIlJVQG1XJ4bHJ-tWt6CFIC1o+52E-L2tOAuAlmvAB-1CB5B6o3B5d8eCh4Tjh4R+u4EVZ1AFyTTpo1LKBNnMXYsBGMWfHDCCxN9ycAJ7EmWbQGJ9gHd+Jkp+p6RB2DRUZ-oGZ7QFZ-Z+5954F-oGF9F-F6l5l7l4V6V5V7V754185+1714N6N7V9N-N-p8t89Zt5jrmEd6-Nt-H0YGIm+zcj+xiBd71Td7J7r4W1JR9-UCdMwBdKgAd5hKNFd9J7AA98p4v-aCCBXPv6d6CuIilsnOIgGMj+9qE-i-zP5e8L4l-UgP-z3Lf8vyK5QAaC2AEXxQBr-c-nqigE4NJycA8YEAKyBP9a+FPdAd7w-7qADGOAo0FgL3K79f+WQZ1v8ztaAscWBA0-kQIgG8BMBoLItp8yYEUC6BSA-Acf2f5oD2BQdUgaQEfZLswOL7AoHwMyB4DBWvAVAeAPf7wggAA

export
interface JigMap {
	ListingManager?: Class<ListingManager>;
	ListingPost?: Class<ListingPost>;
	MediaDispenser?: Class<MediaDispenser>;
	MediaItem?: Class<MediaItem>;
	Offer?: Class<Offer>;
	OfferManager?: Class<OfferManager>;
	PublicListingBoard?: Class<PublicListingBoard>;
	Payment?: Class<Payment>;
}

export {
	ListingPost,
	ListingManager,
	ListingPostRemovalHandle,
	MediaItemData,
	MediaItem,
	MediaDispenser,
	Payment,
	PublicListingBoard,
	Offer,
	OfferManager,
};
