import { parse } from 'qs';
import { useState, useRef, useCallback } from 'react';
import { useLocation } from 'react-router';
import { tuple } from './utils';

export
function useStateRef<T>(initialState: T | (() => T)) {
	const [state, setState] = useState(initialState);
	const ref = useRef(state);
	const dispatch = useCallback((val) => {
		ref.current = typeof val === 'function' ?
			val(ref.current) :
			val;

		setState(ref.current);
	}, []);

	return tuple(state, dispatch, ref);
}

export
function useQuery(): any {
	return parse(useLocation().search, {
		ignoreQueryPrefix: true,
	});
}
