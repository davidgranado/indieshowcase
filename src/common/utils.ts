import { AES, enc } from 'crypto-js';
import { Constants, JigTypes } from './types';
import urlJoin from 'url-join';
import {
	PubKey,
	PrivKey,
	Ecies,
} from 'bsv';
import Run from 'run-sdk';
import { JigLocations } from './constants';
import { Jig, ListingPost, MediaItemData, Offer } from './jigs';

interface UrlArgsMap {
	[key: string]: string | number;
}

const BASE_REQ: RequestInit = {
	headers: {
		Accept: 'application/json, text/plain, */*',
		'Content-Type': 'application/json',
	},
};

export
function exec(fn: () => any | Promise<any>) {
	return fn();
}

export
function pluralize(count: number, noun: string, suffix = 's') {
	return `${noun}${count !== 1 ? suffix : ''}`;
}


export
function tuple<T extends any[]>(...args: T): T {
	return args;
}

export
function objectToArgs(args: number | string | UrlArgsMap) {
	if(typeof args === 'string' || typeof args === 'number') {
		return args;
	}

	return Object
		.keys(args)
		.map(arg => `${arg}=${encodeURIComponent(args[arg])}`)
		.join('&');
}

export
function copyToClipboard(str: string) {
	const el = document.createElement('textarea');
	el.value = str;
	document.body.appendChild(el);
	el.select();
	document.execCommand('copy');
	document.body.removeChild(el);
}

export
async function get<T = any>(path: string, params?: any, options: RequestInit = {}) {
	const paramString = params ? `?${objectToArgs(params)}` : '';
	try {
		const response = await fetch(`${path}${paramString}`, {
			...BASE_REQ,
			...options,
		});
		return await response.json() as T;
	} catch {
		return null;
	}
}

export
async function del(path: string, params?: any, options: RequestInit = {}) {
	const paramString = params ? `?${objectToArgs(params)}` : '';
	const response = await fetch(`${path}${paramString}`, {
		...BASE_REQ,
		...options,
		method: 'DELETE',
	});

	return await response.json();
}

export
async function put<T = any>(path: string, requestBody: UrlArgsMap, options: RequestInit = {}) {
	const response = await fetch(path, {
		...BASE_REQ,
		...options,
		method: 'PUT',
		body: JSON.stringify(requestBody),
	});
	return await response.json() as T;
}

export
async function post<T = any>(path: string, requestBody: UrlArgsMap, options: RequestInit = {}) {
	const response = await fetch(path, {
		...BASE_REQ,
		...options,
		method: 'POST',
		body: JSON.stringify(requestBody),
	});

	return await response.json() as T;
}

export
function range(size: number, startValue = 0) {
	return [ ...Array(size).keys() ].map( i => i + startValue );
}

export
async function filterAsync<T>(list: T[], callback: (i: T) => Promise<boolean>) {
	const fail = Symbol();

	return (
		await Promise.all(
			list.map(async item => (
				await callback(item)
			) ?
				item :
				fail,
			),
		)
	).filter(i=> i !== fail);
}

export
function downloadBlob(filename: string, blob: Blob) {
	const a = document.createElement('a');
	const url = URL.createObjectURL(blob);
	a.href = url;
	a.download = filename;
	a.click();
	URL.revokeObjectURL(url);
}

export
function randomInt(upperBound = 100) {
	return crypto.getRandomValues(new Uint8Array(1))[0] % upperBound;
}

const KEY_CHARSET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

export
function generateKey(length = 8) {
	return range(length)
		.map(
			() => KEY_CHARSET.charAt(randomInt(KEY_CHARSET.length))
		).join('');
}

export
function encryptFile(file: File, key: string, newFileName: string) {
	const reader = new FileReader();

	reader.addEventListener('load', () => {
		const encrypted = encryptAesString(
			reader.result as string,
			key,
		);
		const blob = new Blob([encrypted], {
			type: 'application/octet-stream',
		});

		downloadBlob(newFileName, blob);
	});

	reader.readAsDataURL(file);
}

export
function encryptAesString(msg: string, key: string) {
	return AES.encrypt(
		msg,
		key,
	).toString();
}

export
function decryptFile(file: File, key: string, newFileName: string) {
	const reader = new FileReader();

	reader.addEventListener('load', async () => {
		const decrypted = decryptAesString(
			reader.result as string,
			key,
		);

		const blob = await (await fetch(decrypted)).blob();

		downloadBlob(newFileName, blob);
	});

	reader.readAsBinaryString(file);
}

export
function decryptAesString(encodedString: string, key: string) {
	return AES.decrypt(
		encodedString,
		key,
	).toString(enc.Utf8);
}

export
async function fetchBitcoinFile(txId: string) {
	return (
		await fetch(
			urlJoin(Constants.BITCOIN_FILES_URL, txId)
		)
	).text();
}

const {
	electrumDecrypt,
	electrumEncrypt,
} = Ecies;

export
function encryptWithPublicKey(msg: string, receiverPubKey: string): string {
	return electrumEncrypt(
		Buffer.from(msg),
		PubKey.fromString(receiverPubKey),
		null,
	).toString('base64');
}

export
function decryptWithPrivKey(encryptedBase64Msg: string, privKey: string): string {
	return electrumDecrypt(
		Buffer.from(encryptedBase64Msg, 'base64'),
		PrivKey.Testnet.fromString(privKey),
	).toString();
}

interface Validation {
	key: string;
	msg: string;
}

export
function validateKeys(validation: Validation, privateKey: string) {
	const key = decryptWithPrivKey(
		validation.key,
		privateKey,
	);
	const keyEncryptedMsg = decryptWithPrivKey(
		validation.msg,
		privateKey,
	);
	const msg = decryptAesString(keyEncryptedMsg, key);

	return Constants.VALIDATION_MSG === msg;
}

export
async function isValidPrivateKey(key: string) {
	try {
		return !!PrivKey.Testnet.fromString(key);
	} catch {
		return false;
	}
}

export
function getJigType<T extends Jig>(run: Run | null, type: JigTypes): T | null {
	if(!run) {
		return null;
	}

	// @ts-ignore Figure out
	return run.inventory.jigs.find(
		j => (j.constructor as any).origin === JigLocations[type]
	) || null;
}

export
function getJigTypes<T extends Jig>(run: Run | null, type: JigTypes): T[] {
	if(!run) {
		return [];
	}

	// @ts-ignore Figure out
	return run.inventory.jigs.filter(j => (j.constructor as any).origin === JigLocations[type]);
}

export
type Revokable = Offer | ListingPost;

export
function senderHasRevoked(jig: Revokable) {
	return jig.data.removalHandle.location.endsWith('_d0');
}

export
function notNull<TValue>(value: TValue | null | undefined): value is TValue {
	return value !== null && value !== undefined;
}

export
function reencryptForNewOwner(item: MediaItemData, privateKey: string, newPublicKey: string) {
	const {
		validationMsg,
		key,
	} = item;

	const decryptedValidationMsgStep1 = decryptWithPrivKey(validationMsg, privateKey);
	const decryptedKey = decryptWithPrivKey(key, privateKey);
	const decryptedMsg = decryptAesString(decryptedValidationMsgStep1, decryptedKey);

	const encryptedMsg = encryptAesString(decryptedMsg, decryptedKey);

	return {
		validationMsg: encryptWithPublicKey(encryptedMsg, newPublicKey),
		key: encryptWithPublicKey(decryptedKey, newPublicKey),
	};
}
