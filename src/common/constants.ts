import { AuthState, Config, JigTypes, Store } from './types';

const config: Config = CONFIG;

export
const PK = 'tpubD9fidjoMPrsXabgrrkPnPRn1jckjNLYKDJ3W9te6BZTMS7KDHa6thgPtwxQLpuhrYQ6eKD3rX6KQ6UE1qhCyNosgjwpJC8amwQaJkjZdKY8';

export
const JigLocations = {
	[JigTypes.ListingManager]: 'd61c2fc34280d7ce7ae9cb574d03698a8e9b5700d857e87d001484ca78d7d5db_o3',
	[JigTypes.ListingPost]: 'd61c2fc34280d7ce7ae9cb574d03698a8e9b5700d857e87d001484ca78d7d5db_o2',
	[JigTypes.ListingPostRemovalHandle]: 'd61c2fc34280d7ce7ae9cb574d03698a8e9b5700d857e87d001484ca78d7d5db_o4',
	[JigTypes.MediaDispenser]: '6a42a2c1cfd8a70dbb25524117489af412d8cd3c5086f628c1fa0f5baa259ca4_o2',
	[JigTypes.MediaItem]: '6a42a2c1cfd8a70dbb25524117489af412d8cd3c5086f628c1fa0f5baa259ca4_o1',
	[JigTypes.Offer]: 'd61c2fc34280d7ce7ae9cb574d03698a8e9b5700d857e87d001484ca78d7d5db_o1',
	[JigTypes.OfferManager]: 'd61c2fc34280d7ce7ae9cb574d03698a8e9b5700d857e87d001484ca78d7d5db_o5',
	[JigTypes.OfferRemovalHandle]: 'd61c2fc34280d7ce7ae9cb574d03698a8e9b5700d857e87d001484ca78d7d5db_o6',
	[JigTypes.Payment]: '99cb5b8ec9bcb1554c9ff75ada1d6f9497ebf8e53fa6a4eb3eaeb9f7d5f0dda1_o1',
	[JigTypes.PublicListingBoard]: '2ef11917f4815f498e6e9e4db6165ad17fe20ab0447d8f93f3c1eb6090996aea_o1',
};

export
const DefaultBoardOrigin = '887c6cd08ca6171c8886655ccdf6d79a870f3d2dcc2f87582c1b6a50557e3ba4_o1';

export
const TrustedTransactions = [
	...new Set(
		Object
			.values(JigLocations)
			.flat()
			.map(location => location.split('_')[0])
	),
];

export
const DefaultState: Store = {
	_run: null,
	_anonRun: null,
	_jigs: {},
	loggedInState: AuthState.Unknown,
	rate: 0,
	seed: '',
	user: null,
};

export
const firebaseConfig = {
	apiKey: config.FIREBASE_API_KEY,
	authDomain: config.FIREBASE_AUTH_DOMAIN,
	projectId: config.FIREBASE_PROJECT_ID,
	appId: config.FIREBASE_APP_ID,
};
