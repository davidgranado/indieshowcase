import { exec } from '@common/utils';
import React, { useContext, useEffect, useState } from 'react';
import { Redirect } from 'react-router';
import { ActionsCtx } from '@common/contexts';

export
function Logout() {
	const [done, setDone] = useState(false);
	const {logout} = useContext(ActionsCtx);

	useEffect(() => {
		exec(async () => {
			await logout();
			setDone(true);
		});
	}, []);

	return (
		<>
			{done ? (
				<Redirect to="/"/>
			) : (
				<div>
					Logging Out
				</div>
			)}
		</>
	);
}

export default Logout;
