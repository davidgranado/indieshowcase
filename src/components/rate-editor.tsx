import React, { useContext } from 'react';
import { ActionsCtx, StoreCtx } from '@common/contexts';

export
function RateEditor() {
	const {rate} = useContext(StoreCtx);
	const {setRate} = useContext(ActionsCtx);

	return (
		<>
			Rate: <input
				type="number"
				value={rate}
				onChange={(e) => setRate(+e.target.value)}
			/>
		</>
	);
}
