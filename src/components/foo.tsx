import React, { useContext } from 'react';
import { copyToClipboard } from '@common/utils';
import { ActionsCtx, ComputedCtx } from '@common/contexts';
import { CollectionReview } from './collection-preview';

// const STATS_2_BSV = 100000000;

export
function Foo(): JSX.Element {
	const {
		ownerPubKey,
		ownerPrivKey,
	} = useContext(ComputedCtx);
	const { logout } = useContext(ActionsCtx);

	return (
		<>
			<p>
				Owner Wallet Public Key: {ownerPubKey}
			</p>
			<p>
				<button onClick={() => copyToClipboard(ownerPrivKey)}>Copy Private Key</button>
			</p>
			<p>
				<button onClick={logout}>logout</button>
			</p>

			<CollectionReview/>
		</>
	);
}
