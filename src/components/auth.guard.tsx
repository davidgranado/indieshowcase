import { StoreCtx } from '@common/contexts';
import { AuthState } from '@common/types';
import { IonLoading } from '@ionic/react';
import React, { useContext } from 'react';
import { Redirect } from 'react-router-dom';

interface Props {
	negate?: boolean;
	noredirect?: boolean;
	to?: string;
	children: JSX.Element;
}

export
function AuthGuard(props: Props) {
	const {
		negate,
		noredirect,
		children,
		to = '/login',
	} = props;
	const {
		loggedInState,
	} = useContext(StoreCtx);

	if(loggedInState === AuthState.Unknown) {
		return <IonLoading isOpen/>;
	}

	const isLoggedIn = loggedInState === AuthState.LoggedIn;

	if(
		(negate && isLoggedIn) ||
		(!negate && !isLoggedIn)
	) {
		return noredirect ? (
			null
		) : (
			<Redirect
				to={to}
			/>
		);
	}

	return children;
}
