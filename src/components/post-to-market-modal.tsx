import React, { useContext, useEffect, useState } from 'react';
import {
	IonButton,
	IonButtons,
	IonCheckbox,
	IonCol,
	IonContent,
	IonFab,
	IonFabButton,
	IonGrid,
	IonHeader,
	IonIcon,
	IonInput,
	IonItem,
	IonLabel,
	IonList,
	IonLoading,
	IonModal,
	IonRow,
	IonTextarea,
	IonTitle,
	IonToolbar,
} from '@ionic/react';
import { checkmarkOutline, close } from 'ionicons/icons';
import { ComputedCtx } from '@common/contexts';
import { exec, notNull } from '@common/utils';
import { DefaultBoardOrigin } from '@common/constants';
import { MediaDispenser, MediaItem } from '@common/jigs';

const MAX_MSG_LENGTH = 300;
const MARKET_OWNER = '025aef9718b66ea02860998df580d2e2dd3734dd8b31e7886a701f846cde100e18';

interface FooProps {
	isListed?: boolean;
	selected: boolean;
	overallCount: number;
	batchCount: number;
	item: MediaItem;
	onToggle(): void;
}

function Foo(props: FooProps) {
	const {
		item,
		selected,
		isListed,
		batchCount,
		overallCount,
		onToggle,
	} = props;
	return (
		<IonItem key={item.location}>
			<IonCheckbox
				slot="start"
				disabled={isListed}
				checked={selected || isListed}
				onIonChange={onToggle}
			/>
			<IonLabel>
				<h2>
					Item #{item.data.itemNumber} {!!overallCount && (`/ ${overallCount}`)}
				</h2>
				<p>
					#{item.data.batchItemNumber} in
					print run {item.data.batchNumber} {!!batchCount && (`/ ${batchCount}`)}
				</p>
			</IonLabel>
		</IonItem>
	);
}

interface Props {
	dispenserId: string;
	onClose(): void;
}

export
function PostToMarketModal(props: Props) {
	const {
		dispenserId,
		onClose,
	} = props;
	const {
		userRun,
		listingManager,
	} = useContext(ComputedCtx);
	const {
		inventory,
	} = useContext(ComputedCtx);
	const [dispenser, setDispenser] = useState<MediaDispenser | null>(null);
	const [message, setMessage] = useState('');
	const [price, setPrice] = useState(0);
	const [selectedItems, setSelectedItems] = useState<string[]>([]);
	const [loading, setLoading] = useState(false);
	const sellableList = inventory
		.filter(jig => jig.data.instantiatedBy === dispenserId);
	// @ts-ignore
	const overallCount = dispenser?.data.batchRuns.reduce((sum: number, count: number) => sum+count, 0) | 0;
	const canSave = !!(selectedItems.length && message.trim().length > 3);

	useEffect(() => {
		if(!(userRun && dispenserId)) {
			return;
		}

		exec(async () => {
			const d = await userRun.load(dispenserId);
			await d.sync();
			setDispenser(d);
		});
	}, [dispenserId]);

	function toggleItem(id: string) {
		if(selectedItems.includes(id)) {
			setSelectedItems(
				selectedItems.filter(i => i !== id),
			);
		} else {
			setSelectedItems([
				...selectedItems,
				id,
			]);
		}
	}

	async function handleSubmit() {
		if(!(listingManager && userRun)) {
			return;
		}

		setLoading(true);

		selectedItems
			.map(itemOrigin => inventory.find(i => i.origin === itemOrigin))
			.filter(notNull)
			.map(mediaItem => {
				listingManager.createListing(MARKET_OWNER, {
					message,
					price: +price,
					mediaItem,
					boardOrigin: DefaultBoardOrigin,
				});
			});

		await userRun.sync();

		setLoading(false);

		onClose();
	}

	function isListed(item: MediaItem) {
		return listingManager
			?.data
			.listings
			.some(
				(l: any) => l.data.mediaItem.origin === item.origin
			);
	}

	return (
		<IonModal isOpen={!!dispenserId}>
			<IonHeader>
				<IonToolbar color="primary">
					<IonButtons slot="start">
						<IonButton onClick={onClose}>
							<IonIcon icon={close}/>
						</IonButton>
					</IonButtons>
					<IonTitle>
						Post to market
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent className="ion-padding">
				<IonGrid>
					<IonRow>
						<IonCol>
							<IonItem>
								<IonLabel position="stacked">
									Message
									<em>
										<small>
											({MAX_MSG_LENGTH - message.length} characters left)
										</small>
									</em>
								</IonLabel>
								<IonTextarea
									maxlength={MAX_MSG_LENGTH}
									placeholder="Detail about your post"
									value={message}
									onIonChange={e => setMessage(e.detail.value?.slice(0, MAX_MSG_LENGTH) as string)}
								/>
							</IonItem>
						</IonCol>
					</IonRow>
					<IonRow>
						<IonCol>
							<IonItem>
								<IonLabel position="stacked">
									Price
								</IonLabel>
								<IonInput
									type="number"
									value={price}
									onIonChange={e => setPrice((e.detail.value || 0) as number)}
								/>
							</IonItem>
						</IonCol>
					</IonRow>
					<IonRow>
						<IonCol>
							<IonList>
								{sellableList.map(item => (
									<Foo
										key={item.origin}
										item={item}
										isListed={isListed(item)}
										batchCount={dispenser?.data.batchRuns.length || 0}
										selected={selectedItems.includes(item.origin)}
										overallCount={overallCount}
										onToggle={() => toggleItem(item.origin)}
									/>
								))}
							</IonList>
						</IonCol>
					</IonRow>
				</IonGrid>
				<IonLoading isOpen={loading} />
			</IonContent>
			<IonFab vertical="bottom" horizontal="end" slot="fixed">
				<IonFabButton onClick={handleSubmit} disabled={!canSave}>
					<IonIcon icon={checkmarkOutline} />
				</IonFabButton>
			</IonFab>
		</IonModal>
	);
}
