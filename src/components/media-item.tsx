import React from 'react';
import urljoin from 'url-join';
import { css } from 'linaria';
import {
	IonCardContent,
	IonCardHeader,
	IonCardTitle,
	IonIcon,
	IonRouterLink,
} from '@ionic/react';
import { MediaItem } from '@common/jigs';
import { Constants } from '@common/types';

interface Props {
	count?: number;
	statusIcon?: string;
	statusIconColor?: string;
	description?: string;
	mediaItem: MediaItem;
}

export
function MediaItemCard(props: Props) {
	const {
		count = 0,
		mediaItem,
		description,
		statusIconColor = '',
		statusIcon = '',
	} = props;

	return (
		<div>
			<IonRouterLink
				routerDirection="forward"
				routerLink={`item/${mediaItem.location}`}
			>
				<div style={{position: 'relative'}}>
					<img src={urljoin(Constants.BITCOIN_FILES_URL, mediaItem.data.thumb.tx)} />
					{(count > 1) && (
						<div className={itemCountCls}>
							{count}
						</div>
					)}
				</div>
			</IonRouterLink>
			<IonCardHeader>
				<IonCardTitle>
					{mediaItem.data.title} {statusIcon && <IonIcon icon={statusIcon} color={statusIconColor} />}
				</IonCardTitle>
			</IonCardHeader>
			<IonCardContent>
				<p>
					{description || mediaItem.data.description}
				</p>
			</IonCardContent>
		</div>
	);
}

const size = 30;

const itemCountCls = css`{
	position: absolute;
	bottom: -10px;
	right: 10px;
	border-radius: 50%;
	width: ${size}px;
	height: ${size}px;
	line-height: ${size}px;
	text-align: center;
	color: white;
	font-weight: bold;
	font-size: large;
	background-color: red;
}`;
