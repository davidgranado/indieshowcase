import React, { useContext, useState } from 'react';
import { IonButton, IonContent, IonHeader, IonModal, IonTitle, IonToolbar } from '@ionic/react';
import { ActionsCtx, StoreCtx } from '@common/contexts';
import { copyToClipboard} from '@common/utils';

export
function InitialSeedViewPrompt() {
	const { seed } = useContext(StoreCtx);
	const { setInitialSeedPrompt} = useContext(ActionsCtx);
	const [displaySeed, setDisplaySeed] = useState(false);
	const [dismissed, setDispmissed] = useState(false);

	if(!seed) {
		return null;
	}

	function handleDidDismiss() {
		setInitialSeedPrompt('');
	}

	return (
		<IonModal isOpen={!dismissed} onDidDismiss={handleDidDismiss}>
			<IonHeader>
				<IonToolbar color="primary">
					<IonTitle>
						Recovery seed
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent className="ion-padding">
				<p>
					Below is the recovery seed phrase you will need to recover your
					wallet in the event you lose your password or IndieComicRack
					goes down. We do not have access to this and will not be able
					to recover it for you.
				</p>
				<p>
					Be certain to keep it in a safe place.
				</p>
				{!displaySeed && (
					<IonButton onClick={() => setDisplaySeed(true)}>
						Show Seed
					</IonButton>
				)}
				{displaySeed && (
					<>
						<IonButton onClick={() => setDispmissed(true)}>
							Close
						</IonButton>
						<p>
							{seed} <IonButton size="small" onClick={() => copyToClipboard(seed)}>Copy to Clipboard</IonButton>
						</p>
					</>
				)}
			</IonContent>
		</IonModal>
	);
}
