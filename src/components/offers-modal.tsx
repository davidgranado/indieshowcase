import React, { useContext } from 'react';
import {
	IonButton,
	IonButtons,
	IonContent,
	IonHeader,
	IonIcon,
	IonItem,
	IonLabel,
	IonList,
	IonModal,
	IonTitle,
	IonToolbar,
} from '@ionic/react';
import { alertController } from '@ionic/core';
import { close } from 'ionicons/icons';
import { Offer } from '@common/jigs';
import Run from 'run-sdk';
import { reencryptForNewOwner } from '@common/utils';
import { ComputedCtx, StoreCtx } from '@common/contexts';

interface Props {
	offers: Offer[] | null;
	onClose(): void;
}

export
function OffersModal(props: Props) {
	const {
		offers,
		onClose,
	} = props;
	const {
		_jigs: {
			Payment,
		},
	} = useContext(StoreCtx);
	const {
		ownerPrivKey,
		ownerPubKey,
	} = useContext(ComputedCtx);

	const sortedOffers = offers?.sort((a, b) => a.data.amount - b.data.amount);

	async function offerPrompt(offer: Offer) {
		const alert = await alertController.create({
			header: `${offer.data.amount} BSV`,
			buttons: [
				{
					text: 'Accept',
					async handler() {
						console.log('accepted');
						const tx = new Run.Transaction();
						const {
							requesterPubKey,
							listingPost,
						} = offer.data;
						const mediaItemData = listingPost?.data.mediaItem.data;

						if(!(mediaItemData && listingPost && Payment)) {
							return;
						}

						const {
							validationMsg,
							key,
						} = reencryptForNewOwner(mediaItemData, ownerPrivKey, requesterPubKey);

						tx.update(() =>
							listingPost.data.mediaItem.send(requesterPubKey, key, validationMsg)
						);

						tx.update(() =>
							new Payment(ownerPubKey, offer.data.amount * 100_000_000)
						);
						const y = await tx.export({pay: false});
						console.log(y);
						offer.accept(y);
					},
				},
				{
					text: 'Decline',
					handler() {
						console.log('declined');
						alert.dismiss();
					},
				},
			],
		});

		await alert.present();
	}

	return (
		<IonModal isOpen={!!offers}>
			<IonHeader>
				<IonToolbar color="primary">
					<IonButtons slot="start">
						<IonButton onClick={onClose}>
							<IonIcon icon={close}/>
						</IonButton>
					</IonButtons>
					<IonTitle>
						Offers
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent className="ion-padding">
				<IonList>
					{sortedOffers?.map(o => (
						<IonItem key={o.origin} onClick={() => offerPrompt(o)}>
							<IonLabel>
								{o.data.messages[0]}
							</IonLabel>
							<p>
								{o.data.amount} BSV
							</p>
						</IonItem>
					))}
				</IonList>
			</IonContent>
		</IonModal>
	);
}
