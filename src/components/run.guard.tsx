import { ActionsCtx, ComputedCtx } from '@common/contexts';
import { IonLoading } from '@ionic/react';
import React, { useContext, useEffect } from 'react';

interface Props {
	children: JSX.Element;
}

export
function RunGuard({children}: Props) {
	const {
		anonRun,
		userRun,
	} = useContext(ComputedCtx);
	const {
		initAnonymousUser: initTempUser,
	} = useContext(ActionsCtx);

	useEffect(() => {
		if(!userRun) {
			initTempUser();
		}
	}, []);

	if(!anonRun) {
		return <IonLoading isOpen={true}/>;
	}

	return children;
}
