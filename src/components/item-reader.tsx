import React, { useContext, useEffect, useState } from 'react';
import { Redirect, useParams } from 'react-router-dom';
import urlJoin from 'url-join';
import { ActionsCtx, ComputedCtx, StoreCtx } from '@common/contexts';
import { AuthState, Constants } from '@common/types';
import {
	decryptAesString,
	decryptWithPrivKey,
	exec,
	fetchBitcoinFile,
	range,
	reencryptForNewOwner,
} from '@common/utils';
import { IonContent, IonLoading } from '@ionic/react';
import { MediaItem } from '@common/jigs';

// @ts-ignore
window.reencryptForNewOwner = reencryptForNewOwner;

interface Params {
	txId?: string;
}

interface Props {
	onTitleChange(title: string): void;
}

export
function ItemReader(props: Props) {
	const {
		onTitleChange,
	} = props;
	const {
		txId = '',
	} = useParams<Params>();
	const {
		loggedInState,
	} = useContext(StoreCtx);
	const {
		anonRun,
		ownerPrivKey,
	} = useContext(ComputedCtx);
	const {
		initAnonymousUser,
	} = useContext(ActionsCtx);
	const [topItem, setTopItem] = useState<MediaItem | null>(null);
	const [activeContentIndex, setActiveContentIndex] = useState(0);
	const [loadedContent, setLoadedContent] = useState<any>({});
	const [decryptedKey, setDecryptedKey] = useState('');
	const [recipientPubKey, setRecipientPubKey] = useState('');
	const [batchRuns, setBatchRuns] = useState<number[]>([]);

	const item = topItem?.data || null;
	const activeContent = item?.content[activeContentIndex];
	const contentLength = item?.content.length || 0;
	const isFirst = activeContentIndex === 0;
	const isLast = activeContentIndex === contentLength - 1;
	const {
		batchNumber = 0,
		itemNumber = 0,
		batchItemNumber = 0,
	} = topItem?.data || {};

	useEffect(() => {
		if(loggedInState === AuthState.Unknown) {
			return;
		}

		if(!anonRun) {
			initAnonymousUser();
			return;
		}

		exec(async () => {
			const loadedItem: MediaItem = await anonRun.load(txId);

			await loadedItem.sync();

			setTopItem(loadedItem);
			onTitleChange(loadedItem.data.title);
		});
	}, [loggedInState]);

	useEffect(() => {
		if(!(topItem && anonRun)) {
			return;
		}

		exec(async () => {
			try {
				setDecryptedKey(decryptWithPrivKey(topItem.data.key, ownerPrivKey));
			} catch(e) {
				setDecryptedKey('');
				console.log('not content owner');
			}
			const creator = await anonRun.load(topItem.data.instantiatedBy || '');
			await creator.sync();
			setBatchRuns([...creator.data.batchRuns]);
		});
	}, [topItem]);

	useEffect(() => {
		if(!decryptedKey) {
			return;
		}

		exec(async () => {
			if(!activeContent?.media) {
				return;
			}

			const tx = activeContent.media.tx;

			if(activeContent.media) {
				const content = await fetchBitcoinFile(tx);

				setLoadedContent({
					...loadedContent,
					[tx]:  activeContent.media.encrypted ?
						decryptAesString(content, decryptedKey):
						content,
				});
			}
		});
	}, [activeContent, decryptedKey]);

	if(topItem && topItem.location !== txId) {
		return <Redirect to={`/item/${topItem.location}`}/>;
	}

	async function send() {
		if(!topItem) {
			return;
		}

		const {
			validationMsg,
			key,
		} = reencryptForNewOwner(topItem.data, ownerPrivKey, recipientPubKey);

		console.log(`Sent to address '${recipientPubKey}'`);
		await topItem.send(recipientPubKey, key, validationMsg);
		await topItem.sync();
		setDecryptedKey('');
	}

	if(!(item && activeContent?.media)) {
		return null;
	}

	return (
		<IonContent>
			<IonLoading isOpen={!item}/>
			{(item && (
				<>
					<p>
						{item.description}
					</p>
					<p>
						Print Run: {batchNumber}<br/>
						Number in Run: {batchItemNumber} {!!batchRuns.length && `/ ${batchRuns[batchNumber - 1]}`}<br/>
						Overal Number: {itemNumber} {!!batchRuns.length && `/ ${batchRuns.reduce((sum, count) => sum+count)}`}
					</p>
					<p>
						<button
							disabled={!recipientPubKey}
							onClick={send}
						>
							Send:
						</button>
						<input
							value={recipientPubKey}
							onChange={(e) => setRecipientPubKey(e.target.value)}
						/>
					</p>

					<p>
						<button
							disabled={isFirst}
							onClick={() => setActiveContentIndex(activeContentIndex - 1)}
						>
							Prev
						</button>
						<button
							disabled={isLast}
							onClick={() => setActiveContentIndex(activeContentIndex + 1)}
						>
							Next
						</button>
					</p>

					<p>
						<label>
							Go to Page{' '}
							<select
								value={activeContentIndex}
								onChange={e =>setActiveContentIndex(+e.target.value)}
							>
								{range(contentLength).map(i => (
									<option
										key={i}
										disabled={(item.content[i].media?.encrypted && !decryptedKey) || i === activeContentIndex}
										value={i}
									>
										{item.content[i].title} {!decryptedKey && item.content[i].media?.encrypted && '(private)'}
									</option>
								))}
							</select>
						</label>
					</p>

					<h2>{activeContent.title}</h2>
					{activeContent.description && (
						<p>
							{activeContent.description}
						</p>
					)}
					<p>

					</p>
					{activeContent.media.encrypted && (
						(decryptedKey ? (
							loadedContent[activeContent.media.tx] ? (
								<img src={loadedContent[activeContent.media.tx]} />
							) : (
								<IonLoading isOpen={true}/>
							)
						): (
							'Access Denied'
						))
					)}
					{!activeContent.media.encrypted && (
						<img src={urlJoin(Constants.BITCOIN_FILES_URL, activeContent.media.tx)} />
					)}
				</>
			))}
		</IonContent>
	);
}
