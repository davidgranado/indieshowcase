import React, { useContext, useEffect, useState } from 'react';
import {
	IonButton,
	IonButtons,
	IonCol,
	IonContent,
	IonFab,
	IonFabButton,
	IonGrid,
	IonHeader,
	IonIcon,
	IonInput,
	IonItem,
	IonLabel,
	IonLoading,
	IonModal,
	IonRow,
	IonTitle,
	IonToolbar,
} from '@ionic/react';
import { checkmarkOutline, close } from 'ionicons/icons';
import { ComputedCtx } from '@common/contexts';
import { ListingPost } from '@common/jigs';

interface Props {
	listingPost: ListingPost | null;
	onClose(): void;
}

export
function CreateOfferModal(props: Props) {
	const {
		listingPost,
		onClose,
	} = props;
	const {
		userRun,
		offerManager,
	} = useContext(ComputedCtx);
	const [loading, setLoading] = useState(false);
	const [offerAmount, setOfferAmount] = useState(0);


	useEffect(() => {
		setOfferAmount(listingPost?.data.price || 0);
	}, [listingPost]);

	async function handleSubmit() {
		if(!(offerManager && userRun && listingPost)) {
			return;
		}

		setLoading(true);

		offerManager.createOffer(offerAmount, listingPost.data.mediaItem, listingPost);

		await userRun.sync();

		setLoading(false);

		onClose();
	}

	return (
		<IonModal isOpen={!!listingPost}>
			<IonHeader>
				<IonToolbar color="primary">
					<IonButtons slot="start">
						<IonButton onClick={onClose}>
							<IonIcon icon={close}/>
						</IonButton>
					</IonButtons>
					<IonTitle>
						Create Offer
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent className="ion-padding">
				<IonGrid>
					<IonRow>
						<IonCol>
							<IonItem>
								<IonLabel position="stacked">
									Amount
								</IonLabel>
								<IonInput
									type="number"
									value={offerAmount}
									onIonChange={e => setOfferAmount(+(e.detail.value || 0))}
								/>
							</IonItem>
						</IonCol>
					</IonRow>
				</IonGrid>
				<IonLoading isOpen={loading} />
			</IonContent>
			<IonFab vertical="bottom" horizontal="end" slot="fixed">
				<IonFabButton onClick={handleSubmit}>
					<IonIcon icon={checkmarkOutline} />
				</IonFabButton>
			</IonFab>
		</IonModal>
	);
}
