import React, { useState } from 'react';
import {
	IonButton,
	IonButtons,
	IonContent,
	IonHeader,
	IonIcon,
	IonInput,
	IonLabel,
	IonModal,
	IonTitle,
	IonToolbar,
} from '@ionic/react';
import { close } from 'ionicons/icons';
import localforage from 'localforage';
import { Constants } from '@common/types';
import { decryptAesString } from '@common/utils';
import { presentToast } from '@common/controllers';

interface Props {
	open: boolean;
	onClose(): void;
}

export
function RecoverySeedModal(props: Props) {
	const {
		open,
		onClose,
	} = props;
	const [pw, setPw] = useState('');
	const [seed, setSeed] = useState('');

	async function decode() {
		try {
			const encryptedSeed: string = await localforage.getItem(Constants.ENCRYPTED_OWNER_SEED) || '';
			setSeed(decryptAesString(encryptedSeed, pw));
		} catch {
			presentToast('Incorrect Password');
		}
	}

	return (
		<IonModal isOpen={open}>
			<IonHeader>
				<IonToolbar color="primary">
					<IonButtons slot="start">
						<IonButton onClick={onClose}>
							<IonIcon icon={close}/>
						</IonButton>
					</IonButtons>
					<IonTitle>
						Recovery Seed
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent className="ion-padding">
				{!seed && (
					<>
						<IonLabel>
							Password:
						</IonLabel>
						<IonInput
							value={pw}
							onIonChange={(e) => setPw(e.detail.value || '')}
							type="password"
						/>
						<IonButton onClick={decode}>
							View Seed
						</IonButton>
					</>
				)}
				{seed && (
					<>
						<h2>Seed</h2>
						{seed}
					</>
				)}
			</IonContent>
		</IonModal>
	);
}
