import React, { useState } from 'react';
import { MediaItemCard } from '@components/media-item';
import {
	IonButton,
	IonButtons,
	IonCard,
	IonChip,
	IonFooter,
	IonIcon,
	IonLabel,
	IonToolbar,
} from '@ionic/react';
import { bagCheckOutline, checkmark, close, time, trashOutline } from 'ionicons/icons';
import { pluralize, senderHasRevoked } from '@common/utils';
import { ListingPost, Offer } from '@common/jigs';
import { OffersModal } from './offers-modal';

enum ApprovalStatus {
	APPROVED = 'APPROVED',
	PENDING = 'PENDING',
	REMOVED = 'REMOVED',
}

const ApprovalStatusIcon = {
	[ApprovalStatus.APPROVED]: [checkmark, 'success'],
	[ApprovalStatus.PENDING]: [time, 'warning'],
	[ApprovalStatus.REMOVED]: [close, 'error'],
};

interface Props {
	listingPost: ListingPost;
	offers: Offer[];
	onRemove(): void;
}

export
function ListingCard(props: Props) {
	const {
		listingPost,
		offers,
		onRemove,
	} = props;
	const {
		message,
		price,
		mediaItem,
		approved,
	} = listingPost.data;
	const [modalOpen, setModalOpen] = useState(false);

	const status = approved ?
		ApprovalStatus.APPROVED : (
			senderHasRevoked(listingPost) ?
				ApprovalStatus.REMOVED :
				ApprovalStatus.PENDING
		);

	const [statusIcon, statusColor] = ApprovalStatusIcon[status];

	return (
		<>
			<OffersModal
				offers={modalOpen ? offers : null}
				onClose={() => setModalOpen(false)}
			/>
			<IonCard className="media-item-card">
				<MediaItemCard
					statusIcon={statusIcon}
					statusIconColor={statusColor}
					mediaItem={mediaItem}
					description={`${price}BSV - ${message}`}
				/>
				<IonFooter>
					<IonToolbar>
						<IonButtons>
							{!!offers.length && (
								<IonChip slot="end" color="primary" onClick={() => setModalOpen(true)}>
									<IonLabel>{offers.length} {pluralize(offers.length, 'Offer')}</IonLabel>
									<IonIcon color="success" icon={bagCheckOutline} />
								</IonChip>
							)}
						</IonButtons>
						<IonButtons slot="end">
							<IonButton title="Remove" color="danger" onClick={onRemove}>
								<IonIcon slot="icon-only" icon={trashOutline}/>
							</IonButton>
						</IonButtons>
					</IonToolbar>
				</IonFooter>
			</IonCard>
		</>
	);
}
