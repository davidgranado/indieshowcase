import React, { useContext, useEffect, useState } from 'react';
import { css } from 'linaria';
import {
	bagHandleOutline,
	refresh,
	sendOutline,
	shareOutline,
} from 'ionicons/icons';
import {
	IonButton,
	IonButtons,
	IonCard,
	IonFab,
	IonFabButton,
	IonFooter,
	IonIcon,
	IonLoading,
	IonToolbar,
} from '@ionic/react';
import { ActionsCtx, ComputedCtx, StoreCtx } from '@common/contexts';
import { PostToMarketModal } from './post-to-market-modal';
import { MediaItemCard } from './media-item';

export
function CollectionReview() {
	const {
		inventoryLoading: loading,
	} = useContext(StoreCtx);
	const {
		userRun,
		inventory,
	} = useContext(ComputedCtx);
	const { refreshInventory } = useContext(ActionsCtx);
	const [openInstatiator, setOpenInstatiator] = useState<string>('');
	console.log(inventory);
	const {
		itemsMap,
		countsMap,
	} = inventory.reduce((maps, i) => {
		maps.itemsMap[i.data.instantiatedBy] = i;
		maps.countsMap[i.data.instantiatedBy] = (maps.countsMap[i.data.instantiatedBy] || 0) + 1;
		return maps;
	}, {itemsMap: {}, countsMap: {}} as any);
	const uniqueItems = Object.values<any>(itemsMap);

	useEffect(() => {
		if(loading || !userRun) {
			return;
		}

		refreshInventory();
	}, [userRun]);

	return (
		<>
			<div className={cls}>
				<div>
					<IonLoading isOpen={!!loading} />
					{uniqueItems.map(i => (
						<IonCard
							key={i.location}
							className="media-item-card"
						>
							<MediaItemCard
								mediaItem={i}
								count={countsMap[i.data.instantiatedBy]}
							/>
							<IonFooter>
								<IonToolbar>
									<IonButtons>
										<IonButton title="Send">
											<IonIcon slot="icon-only" icon={sendOutline} />
										</IonButton>
										<IonButton title="Share">
											<IonIcon slot="icon-only" icon={shareOutline} />
										</IonButton>
										<IonButton
											title="Post to Market"
											onClick={() => setOpenInstatiator(i.data.instantiatedBy)}
										>
											<IonIcon slot="icon-only" icon={bagHandleOutline} />
										</IonButton>
									</IonButtons>
								</IonToolbar>
							</IonFooter>
						</IonCard>
					))}
				</div>
				<PostToMarketModal
					dispenserId={openInstatiator}
					onClose={() => setOpenInstatiator('')}
				/>
			</div>
			<IonFab vertical="bottom" horizontal="end" slot="fixed">
				<IonFabButton onClick={refreshInventory}>
					<IonIcon icon={refresh} />
				</IonFabButton>
			</IonFab>
		</>
	);
}

const cls = css`{
}`;
