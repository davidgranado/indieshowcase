import {
	IonContent,
	IonIcon,
	IonItem,
	IonLabel,
	IonMenu,
	IonMenuToggle,
	IonHeader,
	IonToolbar,
	IonTitle,
	IonList,
} from '@ionic/react';
import React, { useContext } from 'react';
import {
	bagOutline,
	homeOutline,
	newspaperOutline,
	personOutline,
} from 'ionicons/icons';
import { ComputedCtx } from '@common/contexts';

function pathProps(currentPath: string, targetUrl: string): any {
	const common = {
		routerDirection: 'root',
		lines: 'none',
	};
	return (currentPath === targetUrl) ? {
		...common,
		detail: true,
	} : {
		...common,
		className: 'selected',
		routerLink: targetUrl,
		detail: false,
	};
}

export
function Menu() {
	const {
		ownerPubKey,
	} = useContext(ComputedCtx);

	return (
		<IonMenu contentId="main" type="overlay">
			<IonHeader>
				<IonToolbar color="primary">
					<IonTitle>Testnet Demo</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent>
				<IonList>

					{!!ownerPubKey && (
						<IonMenuToggle autoHide={false}>
							<IonItem {...pathProps('Home', '/')}>
								<IonIcon slot="start" icon={homeOutline} />
								<IonLabel>
									Library
								</IonLabel>
							</IonItem>
						</IonMenuToggle>
					)}
					<IonMenuToggle autoHide={false}>
						<IonItem {...pathProps('Market', '/market')}>
							<IonIcon slot="start" icon={bagOutline} />
							<IonLabel>
								Market
							</IonLabel>
						</IonItem>
					</IonMenuToggle>
					{!!ownerPubKey && (
						<>
							<IonMenuToggle autoHide={false}>
								<IonItem {...pathProps('Listings', '/listings')}>
									<IonIcon slot="start" icon={newspaperOutline} />
									<IonLabel>
										My Listings
									</IonLabel>
								</IonItem>
							</IonMenuToggle>
							<IonMenuToggle autoHide={false}>
								<IonItem {...pathProps('Profile', '/profile')}>
									<IonIcon slot="start" icon={personOutline} />
									<IonLabel>
										Profile
									</IonLabel>
								</IonItem>
							</IonMenuToggle>
						</>
					)}
				</IonList>
			</IonContent>
		</IonMenu>
	);
}
