import React, { useState } from 'react';

export
function RecoverAccount(): JSX.Element {
	const [ownerKey, setOwnerKey] = useState('');
	const [purseKey, setPurseKey] = useState('');

	return (
		<>
			<p>
				Owner Key:
				<input
					value={ownerKey}
					onChange={e => setOwnerKey(e.target.value.trim()) }
				/>
			</p>
			<p>
				Purse Key:
				<input
					value={purseKey}
					onChange={e => setPurseKey(e.target.value.trim()) }
				/>
			</p>
		</>
	);
}
