import { Config } from '@common/types';

declare global {
	const CONFIG: Config;
	const expect: (arg: any) => any;
	const caller: any;
}
