import { loadingController } from '@ionic/core';
import localforage from 'localforage';
import Run from 'run-sdk';
import { Bip39, Bip32, PubKey } from 'bsv';
import { decryptAesString, encryptAesString, exec, tuple, getJigTypes, getJigType } from '@common/utils';
import { DefaultState, JigLocations } from '@common/constants';
import { AuthState, Constants, FirebaseWalletDoc, JigTypes } from '@common/types';
import { useStateRef } from '@common/hooks';
import { presentToast } from '@common/controllers';
import { useEffect } from 'react';
import { auth, firestore } from '@common/fb';
import { ListingManager, MediaItem, Offer, OfferManager } from '@common/jigs';

export
function useAppState() {
	const [store, setStore, storeRef] = useStateRef(DefaultState);

	useEffect(() => {
		if(storeRef.current.loggedInState === AuthState.Unknown) {
			return;
		}

		exec(async () => {
			if(store.user) {
				if(await localforage.getItem(Constants.OWNER_PRIVATE_KEY)) {
					_initRun();
				}
			} else {
				_clearRun();
			}
		});
	}, [store.user, store.keysInitialized]);

	useEffect(() => {
		auth().onAuthStateChanged(user => {
			console.log('Auth change', user);
			if(user && !storeRef.current.user) {
				setStore({
					...storeRef.current,
					user,
					loggedInState: AuthState.LoggedIn,
				});
			}

			if(!user) {
				setStore({
					...storeRef.current,
					user,
					loggedInState: AuthState.LoggedOut,
				});
			}
		});
	}, []);

	const computed = {
		get ownerPubKey() {
			return this.userRun?.owner?.pubkey || '';
		},
		get ownerPrivKey() {
			return this.userRun?.owner?.privkey || '';
		},
		get isLoggedIn() {
			return !!storeRef.current.user;
		},
		get anonRun() {
			return storeRef.current._run || storeRef.current._anonRun;
		},
		get userRun() {
			if(!storeRef.current?._run) {
				return null;
			}

			if(storeRef.current._run !== Run.instance) {
				storeRef.current._run.activate();
			}

			return storeRef.current._run;
		},
		get marketRun() {
			if(!storeRef.current?._anonRun) {
				return null;
			}

			if(storeRef.current._anonRun !== Run.instance) {
				storeRef.current._anonRun.activate();
			}

			return storeRef.current._anonRun;
		},
		get inventory(): MediaItem[] {
			return getJigTypes<MediaItem>(this.userRun, JigTypes.MediaItem);
		},
		get offers() {
			return getJigTypes<Offer>(this.userRun, JigTypes.Offer);
		},
		get listingManager() {
			return getJigType<ListingManager>(this.userRun, JigTypes.ListingManager);
		},
		get offerManager() {
			return getJigType<OfferManager>(this.userRun, JigTypes.OfferManager);
		},
	};
	const actions = {
		initJigs,
		initAnonymousUser,
		loadRate,
		login,
		logout,
		refreshInventory,
		recovery,
		register,
		sendRestPasswordEmail,
		setRate,
		setInitialSeedPrompt,
	};
	// @ts-ignore
	window.store = storeRef.current;
	// @ts-ignore
	window.computed = computed;
	// @ts-ignore
	window.actions = actions;

	return tuple(
		store,
		computed,
		actions,
	);

	async function _initRun() {
		console.log('_initRun');
		const privKey: string = await localforage.getItem(Constants.OWNER_PRIVATE_KEY) || '';

		const _run = new Run({
			network: 'test',
			trust: '*',
			purse: 'cTxJE7H9pzXGCSUcBwfCY3ncyApnRfuESKqUpBycBvopXNuvZYZi',
			owner: privKey,
		});

		setStore({
			...storeRef.current,
			_run,
		});

		await refreshInventory();
		console.log('_initRun done');
	}

	async function _clearRun() {
		console.log('_clearRun');
		setStore({
			...DefaultState,
		});
		console.log('_clearRun done');
	}

	async function initJigs() {
		console.log('initJigs');
		const {
			_run,
		} = storeRef.current;

		// @ts-ignore
		if(window.loadedJigs) {
			return;
		}

		if(!_run) {
			return;
		}

		setStore({
			...storeRef.current,
		});

		const loadedJigs: any = {};
		// @ts-ignore
		window.loadedJigs = loadedJigs;

		await Promise.all(
			Object
				.entries(JigLocations)
				.map(async ([jigType, location]) => {
					const j = await _run.load(location);
					await j.sync();
					loadedJigs[jigType] = j;
					return j;
				}),
		);

		setStore({
			...storeRef.current,
			_jigs: loadedJigs,
		});

		console.log('initJigs done');
		// @ts-ignore
		window['loadedJigs'] = loadedJigs;
	}

	async function refreshInventory() {
		console.log('refreshInventory');
		const {
			_jigs,
		} = storeRef.current;
		const _run = computed.anonRun;

		if(!_run) {
			return;
		}

		setStore({
			...storeRef.current,
			inventoryLoading: true,
		});

		if(!_jigs.MediaItem) {
			await initJigs();
		}

		await _run.inventory.sync();

		if(_jigs.OfferManager && !_run.inventory.jigs.some(j => (j.constructor as any).origin === JigLocations[JigTypes.OfferManager])) {

			new _jigs.OfferManager(computed.ownerPubKey);
		}

		if(_jigs.ListingManager && !_run.inventory.jigs.some(j => (j.constructor as any).origin === JigLocations[JigTypes.ListingManager])) {
			new _jigs.ListingManager(computed.ownerPubKey);
		}

		setStore({
			...storeRef.current,
			inventoryLoading: false,
		});

		console.log('refreshInventory done');
	}

	async function setRate(newRate: number) {
		console.log('setRate');
		setStore({
			...storeRef.current,
			rate: newRate,
		});
		await localforage.setItem(Constants.RATE_KEY, newRate);
		console.log('setRate');
	}

	async function setInitialSeedPrompt(seed: string) {
		setStore({
			...storeRef.current,
			seed,
		});
	}

	async function loadRate() {
		console.log('loadRate');
		const savedRate = await localforage.getItem<number>(Constants.RATE_KEY);

		if(savedRate) {
			setRate(savedRate);
		}
		console.log('loadRate done');
	}

	async function logout() {
		console.log('logout');
		await localforage.clear();
		await auth().signOut();
		setStore({
			...DefaultState,
		});
		console.log('logout done');
	}


	async function login(email: string, password: string) {
		console.log('login');
		const loader = await loadingController.create({});
		await loader.present();

		try {
			const cred = await auth().signInWithEmailAndPassword(email, password);
			let ownerKey: string = (await localforage.getItem(Constants.OWNER_PRIVATE_KEY)) || '';

			if(!ownerKey) {
				const doc = await firestore().doc(`/wallets/${cred.user?.uid}`).get();
				const {
					ecnryptedOwnerPrivKey,
					ecnryptedOwnerSeed,
				} = doc.data() as FirebaseWalletDoc;

				ownerKey = decryptAesString(ecnryptedOwnerPrivKey, password);

				await localforage.setItem(Constants.OWNER_PRIVATE_KEY, ownerKey);
				await localforage.setItem(Constants.ENCRYPTED_OWNER_SEED, ecnryptedOwnerSeed);
			}

			setStore({
				...storeRef.current,
				keysInitialized: true,
			});

		} catch (e) {
			presentToast(e.message);
		}

		await loader.dismiss();
		console.log('login done');
	}

	function sendRestPasswordEmail(email: string) {
		return auth().sendPasswordResetEmail(email);
	}

	async function register(email: string, password: string) {
		console.log('register');
		const loader = await loadingController.create({});
		await loader.present();

		try {
			const b39 = Bip39.fromRandom();
			const b32 = Bip32.Testnet.fromSeed(b39.seed);
			const ownerPrivKeyStr = b32.privKey.toString();
			const ownerPubKey = PubKey.fromPrivKey(b32.privKey);

			const ecnryptedOwnerPrivKey = encryptAesString(ownerPrivKeyStr, password);
			const ecnryptedOwnerSeed = encryptAesString(b39.mnemonic, password);

			await localforage.setItem(Constants.OWNER_PRIVATE_KEY, ownerPrivKeyStr);
			await localforage.setItem(Constants.ENCRYPTED_OWNER_SEED, ecnryptedOwnerSeed);
			const cred = await auth().createUserWithEmailAndPassword(email, password);

			firestore().doc(`/wallets/${cred.user?.uid}`).set({
				ecnryptedOwnerPrivKey,
				ecnryptedOwnerSeed,
				ownerPubKey: ownerPubKey.toString(),
			});

			setStore({
				...storeRef.current,
				keysInitialized: true,
			});

			setInitialSeedPrompt(b39.mnemonic);

		} catch (e) {
			presentToast(e.message);
		}

		await loader.dismiss();
		console.log('register done');
	}

	async function recovery(email: string, password: string, seed: string, oobCode: string) {
		console.log('recovery');
		const loader = await loadingController.create({});
		await loader.present();

		try {
			const b39 = Bip39.fromString(seed);
			const b32 = Bip32.Testnet.fromSeed(b39.toSeed());
			const ownerPrivKeyStr = b32.privKey.toString();
			const ownerPubKey = PubKey.fromPrivKey(b32.privKey);

			const ecnryptedOwnerPrivKey = encryptAesString(ownerPrivKeyStr, password);
			const ecnryptedOwnerSeed = encryptAesString(b39.mnemonic, password);

			await localforage.setItem(Constants.OWNER_PRIVATE_KEY, ownerPrivKeyStr);
			await localforage.setItem(Constants.ENCRYPTED_OWNER_SEED, ecnryptedOwnerSeed);
			const a = auth();
			await a.confirmPasswordReset(oobCode, password);
			const cred = await a.signInWithEmailAndPassword(email, password);

			firestore().doc(`/wallets/${cred.user?.uid}`).set({
				ecnryptedOwnerPrivKey,
				ecnryptedOwnerSeed,
				ownerPubKey: ownerPubKey.toString(),
			});

			setStore({
				...storeRef.current,
				keysInitialized: true,
			});

		} catch (e) {
			presentToast(e.message);
		}

		await loader.dismiss();
		console.log('recovery done');

	}


	// Used for actions like viewing items without
	// needing to persist a user item reader
	function initAnonymousUser() {
		console.log('initTempUser');

		const anonRun = new Run({
			network: 'test',
			trust: '*',
		});

		setStore({
			...storeRef.current,
			_anonRun: anonRun,
		});
		initJigs();
		console.log('initTempUser done');

		return anonRun;
	}
}
