import React, { useState } from 'react';
import { IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import {ItemReader} from '@components/item-reader';

export
function ItemPage() {
	const [title, setTitle] = useState('Item');

	return (
		<IonPage>
			<IonHeader>
				<IonToolbar color="primary">
					<IonButtons slot="start">
						<IonMenuButton />
					</IonButtons>
					<IonTitle>
						{title}
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent scrollX className="ion-padding">
				<ItemReader onTitleChange={setTitle} />
			</IonContent>
		</IonPage>
	);
}

export default ItemPage;
