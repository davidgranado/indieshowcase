import React, { useContext, useEffect, useState } from 'react';
import { ActionsCtx, ComputedCtx, StoreCtx } from '@common/contexts';
import { groupBy } from 'lodash';
import {
	IonButtons,
	IonContent,
	IonFab,
	IonFabButton,
	IonHeader,
	IonIcon,
	IonLoading,
	IonMenuButton,
	IonPage,
	IonTitle,
	IonToolbar,
} from '@ionic/react';
import { css } from '@linaria/core';
import { exec, senderHasRevoked } from '@common/utils';
import { ListingPost } from '@common/jigs';
import { ListingCard } from '@components/listing-card';
import { refresh } from 'ionicons/icons';

// TODO What's the deal here when trying to access location
function listingNotRemoved(l: ListingPost) {
	try {
		return !senderHasRevoked(l);
	} catch {
		return false;
	}
}

function Foo() {
	const [loading, setLoading] = useState(false);
	const {
		inventoryLoading,
	} = useContext(StoreCtx);
	const {
		listingManager,
		userRun,
		offers,
	} = useContext(ComputedCtx);
	const {
		refreshInventory,
	} = useContext(ActionsCtx);

	useEffect(() => {
		exec(async () => {
			if(!inventoryLoading) {
				await refreshInventory();
			}
		});
	}, [userRun]);

	useEffect(() => {
		if(!listingManager) {
			return;
		}

		refreshListings();

	}, [listingManager]);

	if(!listingManager) {
		return null;
	}

	const listings = listingManager.data.listings.filter(listingNotRemoved);
	const groupedListings = groupBy(listings, l => l.data.boardOrigin);

	async function removeItem(listing: ListingPost) {
		if(!listingManager) {
			return;
		}

		setLoading(true);
		//@ts-ignore
		await listingManager.removeListingPost(listing);
		await listingManager.sync();

		setTimeout(() => setLoading(false), 1000);
	}

	async function refreshListings() {
		if(!(userRun && listingManager)) {
			return;
		}

		setLoading(true);

		await Promise.all([
			listingManager.sync({forward: true, inner: true}),
			...offers.map(o => o.sync({forward: true, inner: true})),
			refreshInventory(),
		]);

		await Promise.all(
			offers.map(o => {
				if(senderHasRevoked(o)) {
					return o.destroy();
				}
			})
		);

		setLoading(false);
	}

	return (
		<>
			<IonLoading isOpen={loading} />
			{Object.entries(groupedListings).map(([boardOrigin, ls]) => (
				<div key={boardOrigin} className={cls}>
					<h3>{boardOrigin}</h3>
					{ls.map(l => (
						<ListingCard
							offers={offers.filter(o => o.data.listingPost?.origin === l.origin)}
							key={l.location}
							listingPost={l}
							onRemove={() => removeItem(l)}
						/>
					))}
				</div>
			))}
			<IonFab vertical="bottom" horizontal="end" slot="fixed">
				<IonFabButton onClick={refreshListings}>
					<IonIcon icon={refresh} />
				</IonFabButton>
			</IonFab>
		</>
	);
}

export
function ListingsPage() {
	return (
		<IonPage>
			<IonHeader>
				<IonToolbar color="primary">
					<IonButtons slot="start">
						<IonMenuButton />
					</IonButtons>
					<IonTitle>
						Your market listings
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent scrollX className="ion-padding">
				<Foo/>
			</IonContent>
		</IonPage>
	);
}

export default ListingsPage;

const cls = css`{
}`;
