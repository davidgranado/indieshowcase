import React from 'react';
import {
	IonContent,
	IonButtons,
	IonHeader,
	IonMenuButton,
	IonPage,
	IonTitle,
	IonToolbar,
} from '@ionic/react';
import { CollectionReview } from '@components/collection-preview';

export
function HomePage() {
	return (
		<IonPage>
			<IonHeader>
				<IonToolbar color="primary">
					<IonButtons slot="start">
						<IonMenuButton />
					</IonButtons>
					<IonTitle>
						Home
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent scrollX className="ion-padding">
				<CollectionReview/>
			</IonContent>
		</IonPage>
	);
}

export default HomePage;
