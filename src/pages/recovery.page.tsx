import React, { useContext, useEffect, useState } from 'react';
import {
	IonHeader,
	IonToolbar,
	IonTitle,
	IonPage,
	IonContent,
	IonCard,
	IonCardContent,
	IonInput,
	IonItem,
	IonLabel,
	IonGrid,
	IonRow,
	IonCol,
	IonCardHeader,
	IonIcon,
	IonFooter,
	IonButtons,
	IonButton,
	IonLoading,
} from '@ionic/react';
import { personOutline } from 'ionicons/icons';
import { auth } from '@common/fb';
import { ActionsCtx } from '@common/contexts';
import { useQuery } from '@common/hooks';
import { exec } from '@common/utils';
import { Redirect } from 'react-router';

export
function RecoveryPage() {
	const { oobCode } = useQuery();
	const { recovery } = useContext(ActionsCtx);
	const [loading, setLoading] = useState(true);
	const [errorMsg, setErrorMsg] = useState(false);
	const [linkVerified, setLinkVerified] = useState(false);
	const [seed, setSeed] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [pwConfirmation, setPwConfirmation] = useState('');
	const [recoverComplete, setRecoveryComplete] = useState(false);
	const passwordsMatch = password === pwConfirmation;
	const isValid = !!(email && password && passwordsMatch);

	useEffect(() => {
		exec(async() => {
			try {
				setEmail(await auth().verifyPasswordResetCode(oobCode as string));

				setLinkVerified(true);
			} catch (e) {
				setErrorMsg(e.message);
			}

			setLoading(false);
		});
	});

	async function handleReset() {
		await recovery(email, password, seed, oobCode);

		setRecoveryComplete(true);
	}

	if(recoverComplete) {
		return (
			<Redirect to="/" />
		);
	}

	return (
		<IonPage>
			<IonHeader>
				<IonToolbar color="primary">
					<IonTitle>
						Account Recovery
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent className="ion-padding">
				<IonLoading isOpen={loading}/>
				{!!errorMsg && errorMsg}
				{linkVerified && (
					<IonGrid fixed>
						<IonRow>
							<IonCol sizeLg="6" offsetLg="3">
								<IonCard>
									<IonCardHeader>
										Recover
									</IonCardHeader>
									<IonCardContent>
										<IonItem>
											<IonLabel position="stacked">
												Email
											</IonLabel>
											<IonInput
												value={email}
												onIonChange={(e) => setEmail(e.detail.value || '')}
											/>
										</IonItem>
										<IonItem>
											<IonLabel position="stacked">
												Wallet Recovery Seed
											</IonLabel>
											<IonInput
												type="text"
												value={seed}
												onIonChange={(e) => setSeed(e.detail.value || '')}
											/>
										</IonItem>
										<IonItem>
											<IonLabel position="stacked">
												New Password
											</IonLabel>
											<IonInput
												type="password"
												value={password}
												onIonChange={(e) => setPassword(e.detail.value || '')}
											/>
										</IonItem>
										<IonItem>
											<IonLabel position="stacked">
												Confirm New Password
											</IonLabel>
											<IonInput
												type="password"
												value={pwConfirmation}
												onIonChange={(e) => setPwConfirmation(e.detail.value || '')}
											/>
										</IonItem>
										{!passwordsMatch && (
											<p>
												<em>
													Passwords do not match
												</em>
											</p>
										)}
									</IonCardContent>

									<IonFooter>
										<IonToolbar>
											<IonButtons slot="end">
												<IonButton
													color="primary"
													disabled={!isValid}
													onClick={handleReset}
												>
													<IonIcon icon={personOutline} />
													Reset Password
												</IonButton>
											</IonButtons>
										</IonToolbar>
									</IonFooter>
								</IonCard>
							</IonCol>
						</IonRow>
					</IonGrid>
				)}
			</IonContent>
		</IonPage>
	);
}

export default RecoveryPage;
