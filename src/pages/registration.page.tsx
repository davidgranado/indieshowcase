import React, { useContext, useState } from 'react';
import {
	IonHeader,
	IonToolbar,
	IonTitle,
	IonPage,
	IonContent,
	IonCard,
	IonCardContent,
	IonInput,
	IonItem,
	IonLabel,
	IonGrid,
	IonRow,
	IonCol,
	IonCardHeader,
	IonIcon,
	IonFooter,
	IonButtons,
	IonButton,
} from '@ionic/react';
import { logInOutline, personOutline } from 'ionicons/icons';
import { ActionsCtx } from '@common/contexts';

export
function RegistrationPage() {
	const { register } = useContext(ActionsCtx);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [pwConfirmation, setPwConfirmation] = useState('');
	const passwordsMatch = password === pwConfirmation;
	const isValid = !!(email && password && passwordsMatch);

	return (
		<IonPage>
			<IonHeader>
				<IonToolbar color="primary">
					<IonTitle>
						Register
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent className="ion-padding">
				<IonGrid fixed>
					<IonRow>
						<IonCol sizeLg="6" offsetLg="3">
							<IonCard>
								<IonCardHeader>
									<IonIcon icon={logInOutline}/>
									Register
								</IonCardHeader>
								<IonCardContent>
									<IonItem>
										<IonLabel position="stacked">
											Email
										</IonLabel>
										<IonInput
											value={email}
											onIonChange={(e) => setEmail(e.detail.value || '')}
										/>
									</IonItem>
									<IonItem>
										<IonLabel position="stacked">
											Password
										</IonLabel>
										<IonInput
											type="password"
											value={password}
											onIonChange={(e) => setPassword(e.detail.value || '')}
										/>
									</IonItem>
									<IonItem>
										<IonLabel position="stacked">
											Confirm Password
										</IonLabel>
										<IonInput
											type="password"
											value={pwConfirmation}
											onIonChange={(e) => setPwConfirmation(e.detail.value || '')}
										/>
									</IonItem>
									{!passwordsMatch && (
										<p>
											<em>
												Passwords do not match
											</em>
										</p>
									)}
								</IonCardContent>

								<IonFooter>
									<IonToolbar>
										<IonButtons slot="start">
											<IonButton
												routerLink="/login"
												routerDirection="none"
											>
												<IonIcon icon={logInOutline}/>
												I have an account
											</IonButton>
										</IonButtons>
										<IonButtons slot="end">
											<IonButton
												color="primary"
												disabled={!isValid}
												onClick={() => register(email, password)}
											>
												<IonIcon icon={personOutline} />
												Register
											</IonButton>
										</IonButtons>
									</IonToolbar>
								</IonFooter>
							</IonCard>
						</IonCol>
					</IonRow>
				</IonGrid>
			</IonContent>
		</IonPage>
	);
}

export default RegistrationPage;
