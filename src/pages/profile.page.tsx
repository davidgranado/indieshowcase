import React, { useContext, useState } from 'react';
import {
	IonButton,
	IonButtons,
	IonContent,
	IonHeader,
	IonMenuButton,
	IonPage,
	IonRouterLink,
	IonTitle,
	IonToolbar,
} from '@ionic/react';
import { ComputedCtx, StoreCtx } from '@common/contexts';
import { RecoverySeedModal } from '@components/recovery-seed-modal';

export
function ProfilePage() {
	const {
		ownerPrivKey,
		ownerPubKey,
	} = useContext(ComputedCtx);
	const {
		user,
	} = useContext(StoreCtx);
	const [recoverySeedModalOpen, setRecoverySeedModalOpen] = useState(false);

	return (
		<IonPage>
			<IonHeader>
				<IonToolbar color="primary">
					<IonButtons slot="start">
						<IonMenuButton />
					</IonButtons>
					<IonTitle>
						Profile
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent className="ion-padding">
				{!!(ownerPrivKey && ownerPubKey) && (
					<>
						<p>
							Owner Wallet Public Key: {ownerPubKey}
						</p>
						<p>
							<IonButton onClick={() => setRecoverySeedModalOpen(true)}>
								View recovery seed
							</IonButton>
						</p>
						{user && (
							<p>
								Logged in as {user.email}
							</p>
						)}
						<p>
							<IonRouterLink routerLink="/logout" routerDirection="back">
								Log Out
							</IonRouterLink>
						</p>
					</>
				)}
				<RecoverySeedModal open={recoverySeedModalOpen} onClose={() => setRecoverySeedModalOpen(false)} />
			</IonContent>
		</IonPage>
	);
}

export default ProfilePage;
