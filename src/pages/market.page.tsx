import React, { useContext, useEffect, useState } from 'react';
import { ActionsCtx, ComputedCtx, StoreCtx } from '@common/contexts';
import { AuthState } from '@common/types';
import { MediaItemCard } from '@components/media-item';
import { CreateOfferModal } from '@components/create-offer-modal';
import {
	IonButton,
	IonButtons,
	IonCard,
	IonChip,
	IonContent,
	IonFab,
	IonFabButton,
	IonFooter,
	IonHeader,
	IonIcon,
	IonLabel,
	IonLoading,
	IonMenuButton,
	IonPage,
	IonTitle,
	IonToolbar,
} from '@ionic/react';
import { css } from '@linaria/core';
import { cartOutline, closeCircle, heartOutline, refresh } from 'ionicons/icons';
import { DefaultBoardOrigin } from '@common/constants';
import { ListingPost, OfferManager, PublicListingBoard } from '@common/jigs';
import { senderHasRevoked } from '@common/utils';

interface FooParams {
	listing: ListingPost;
	onClick(): void;
}

function Foo(params: FooParams) {
	const {
		listing,
		onClick,
	} = params;
	const listingMediaItemOrigin = listing.data.mediaItem.origin;
	const {
		offerManager,
	} = useContext(ComputedCtx);
	const [loading, setLoading] = useState(false);

	const listingOffer = (offerManager as OfferManager)?.data.offers[listingMediaItemOrigin];

	async function removeOffer(mediaItemOrigin: string) {
		setLoading(true);
		console.log(mediaItemOrigin);
		await offerManager?.rescindOffer(mediaItemOrigin);
		setLoading(false);
	}

	return (
		<>
			<IonLoading isOpen={loading}/>
			<IonCard
				key={listing.location}
				className={`${cls} media-item-card`}
			>
				<MediaItemCard
					mediaItem={listing.data.mediaItem}
					description={`${listing.data.price}BSV - ${listing.data.message}`}
				/>
				<IonFooter>
					<IonToolbar>
						<IonButtons>
							{!listingOffer && (
								<IonButton title="Make Offer" onClick={onClick}>
									<IonIcon slot="icon-only" icon={cartOutline} />
								</IonButton>
							)}
							<IonButton title="Favorite">
								<IonIcon slot="icon-only" icon={heartOutline} />
							</IonButton>
						</IonButtons>
						{listingOffer && (
							<IonChip slot="end" color="primary" onClick={() => removeOffer(listing.data.mediaItem.origin)}>
								<IonLabel>Pending Offer {listingOffer.data.amount} BSV</IonLabel>
								<IonIcon color="danger" icon={closeCircle} />
							</IonChip>
						)}

					</IonToolbar>
				</IonFooter>
			</IonCard>
		</>
	);
}

function listingNotRemoved(l: ListingPost) {
	try {
		return !senderHasRevoked(l);
	} catch {
		return false;
	}
}

export
function MarketPage() {
	const {
		loggedInState,
	} = useContext(StoreCtx);
	const {
		anonRun,
	} = useContext(ComputedCtx);
	const {
		initAnonymousUser,
	} = useContext(ActionsCtx);
	const [loading, setLoading] = useState(false);
	const [activeListing, setActiveListing] = useState<ListingPost | null>(null);
	const [marketBoard, setMarketBoard] = useState<PublicListingBoard|null>(null);
	const listings: ListingPost[] = marketBoard?.data.approvedListings.filter(listingNotRemoved) || [];

	useEffect(() => {
		if(loggedInState === AuthState.Unknown) {
			return;
		}

		if(!(anonRun)) {
			initAnonymousUser();
			return;
		}

		refreshInventory();
	}, [anonRun]);

	async function refreshInventory() {
		if(!anonRun) {
			return;
		}

		setLoading(true);
		const mb = await anonRun.load(marketBoard?.location || DefaultBoardOrigin);
		await mb.sync({forward: true, inner: true});
		setMarketBoard(mb);
		setLoading(false);
	}

	return (
		<IonPage>
			<IonHeader>
				<IonToolbar color="primary">
					<IonButtons slot="start">
						<IonMenuButton />
					</IonButtons>
					<IonTitle>
						Market {marketBoard && `- ${marketBoard.data.name}`}
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent scrollX className="ion-padding">
				<IonLoading isOpen={loading} />
				<p>
					{marketBoard?.data.description}
				</p>
				{listings.map(l => (
					<Foo
						key={l.location}
						listing={l}
						onClick={() => setActiveListing(l)}
					/>
				))}
				<CreateOfferModal
					listingPost={activeListing}
					onClose={() => setActiveListing(null)}
				/>
			</IonContent>
			<IonFab vertical="bottom" horizontal="end" slot="fixed">
				<IonFabButton onClick={refreshInventory}>
					<IonIcon icon={refresh} />
				</IonFabButton>
			</IonFab>
		</IonPage>
	);
}

export default MarketPage;

const cls = css`{
}`;
