import React, { useContext, useState } from 'react';
import {
	IonHeader,
	IonToolbar,
	IonTitle,
	IonPage,
	IonContent,
	IonCard,
	IonCardContent,
	IonInput,
	IonItem,
	IonLabel,
	IonGrid,
	IonRow,
	IonCol,
	IonCardHeader,
	IonIcon,
	IonFooter,
	IonButtons,
	IonButton,
} from '@ionic/react';
import { Key } from 'ts-key-enum';
import { logInOutline, personOutline } from 'ionicons/icons';
import { ActionsCtx } from '@common/contexts';
import { presentToast } from '@common/controllers';

export
function LoginPage() {
	const {
		login,
		sendRestPasswordEmail,
	} = useContext(ActionsCtx);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const isValid = !!(email && password);

	async function handlePasswordReset() {
		if(!email) {
			presentToast('Enter an email address');
			return;
		}

		try {
			await sendRestPasswordEmail(email);
			presentToast(`Recovery email sent to "${email}"`);
		} catch(e) {
			console.log(e);
			presentToast(e.message);
		}
	}

	async function handleLogin() {
		if(!isValid) {
			return;
		}

		login(email, password);
	}

	async function handleKeyup(key: string) {
		if(key === Key.Enter) {
			handleLogin();
		}
	}

	return (
		<IonPage>
			<IonHeader>
				<IonToolbar color="primary">
					<IonTitle>
						Login
					</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent>
				<IonGrid fixed>
					<IonRow>
						<IonCol sizeLg="6" offsetLg="3">
							<IonCard>
								<IonCardHeader>
									Login
								</IonCardHeader>
								<IonCardContent>
									<IonItem>
										<IonLabel position="stacked">
											Email
										</IonLabel>
										<IonInput
											value={email}
											onKeyUp={(e) => handleKeyup(e.key)}
											onIonChange={(e) => setEmail(e.detail.value || '')}
										/>
									</IonItem>
									<IonItem>
										<IonLabel position="stacked">
											Password
										</IonLabel>
										<IonInput
											type="password"
											value={password}
											onKeyUp={(e) => handleKeyup(e.key)}
											onIonChange={(e) => setPassword(e.detail.value || '')}
										/>
									</IonItem>
									<p>
										<IonButton
											fill="clear"
											color="primary"
											size="small"
											onClick={handlePasswordReset}
										>
											<em>
												Forgot Password
											</em>
										</IonButton>
									</p>
								</IonCardContent>

								<IonFooter>
									<IonToolbar>
										<IonButtons slot="start">
											<IonButton
												routerLink="/register"
												routerDirection="none"
											>
												<IonIcon icon={personOutline}/>
												Create an account
											</IonButton>
										</IonButtons>
										<IonButtons slot="end">
											<IonButton
												color="primary"
												disabled={!isValid}
												onClick={handleLogin}
											>
												<IonIcon icon={logInOutline}/>
												Login
											</IonButton>
										</IonButtons>
									</IonToolbar>
								</IonFooter>
							</IonCard>
						</IonCol>
					</IonRow>
				</IonGrid>
			</IonContent>
		</IonPage>
	);
}

export default LoginPage;
