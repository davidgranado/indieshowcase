import React, { useState } from 'react';
import { decryptFile, encryptFile, generateKey } from '@common/utils';
import { css } from 'linaria';

export
function EncodeFilePage(): JSX.Element {
	const [key, setKey]= useState('');
	const [newFileName, setNewFileName]= useState('');
	const [file, setFile] = useState<File | null>(null);
	const canSave = !!(newFileName && key && file);

	return (
		<div className={cls}>
			<p>
				New File Name:
				<input
					value={newFileName}
					onChange={e => setNewFileName(e.target.value.trim())}
				/>
			</p>
			<p>
				Key:
				<input
					className="key-input"
					value={key}
					onChange={e => setKey(e.target.value.trim())}
				/>
				<button onClick={() => setKey(generateKey(32))}>
					Random Key
				</button>
			</p>
			<input
				type="file"
				onChange={e => e.target.files?.[0] && setFile(e.target.files[0])}
			/>
			<button
				disabled={!canSave}
				onClick={() => file && encryptFile(file, key, newFileName)}
			>
				Encrypt
			</button>
			<button
				disabled={!canSave}
				onClick={() => file && decryptFile(file, key, newFileName)}
			>
				Decrypt
			</button>
		</div>
	);
}

const cls = css`{
	.key-input {
		width: 300px;
	}
}`;

export default EncodeFilePage;
