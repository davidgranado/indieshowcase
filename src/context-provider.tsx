import React, { FC, useEffect } from 'react';
import { ActionsCtx, ComputedCtx, StoreCtx} from '@common/contexts';
import { exec } from '@common/utils';
import { useAppState } from './useAppState';

export
const ContextProvider: FC = (props) => {
	const [state, computed, actions] = useAppState();

	useEffect(() => {
		exec(async () => {
			actions.loadRate();
		});
	}, []);

	return (
		<StoreCtx.Provider value={state}>
			<ComputedCtx.Provider value={computed}>
				<ActionsCtx.Provider value={actions}>
					{props.children}
				</ActionsCtx.Provider>
			</ComputedCtx.Provider>
		</StoreCtx.Provider>
	);
};
